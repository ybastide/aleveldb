﻿// Copyright (c) 2012-2013 The ALevelDb Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file. See the AUTHORS file for names of contributors.

using System;
using System.Collections.Generic;
using System.Text;
using ALevelDb;
using NUnit.Framework;
using TswBase;

namespace TestLevelDb
{
    [TestFixture]
    public class TestIter2
    {
        readonly LevelDbOptions _options = new LevelDbOptions { CreateIfMissing = true };

        [Test]
        public void Test01()
        {
            LevelDb.DestroyIfExists("test.leveldb");
            using (var db = new LevelDb("test.leveldb", _options))
            {
                var list = new List<int>();
                const int count = 1000;
                for (int i = 0; i < count; i++)
                    list.Add(i);
                G.ListRandomize(list, new Random());

                foreach (var i in list)
                {
                    db.Put(i.ToString("000"), i);
                }
                int prev = -1;
                int j = 0;
                foreach (KeyValuePair<byte[], byte[]> kv in db.GetIterator2("000"))
                {
                    //string key = Encoding.UTF8.GetString(kv.Key);
                    int value = BitConverter.ToInt32(kv.Value, 0);
                    //if (value < prev)
                    //{
                    //    Console.WriteLine(value);
                    //}
                    //Assert.Greater(value, prev);
                    Assert.AreEqual(prev + 1, value);
                    prev = value;
                    j++;
                }
                Assert.AreEqual(count, j);
                GC.Collect(2, GCCollectionMode.Forced, true);
            }
        }
    }
}
