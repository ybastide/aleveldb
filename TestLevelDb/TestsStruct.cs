﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using ALevelDb;
using NUnit.Framework;

namespace TestLevelDb
{
    [TestFixture]
    public class TestsStruct
    {
        [StructLayout(LayoutKind.Sequential)]
        struct MyKey
        {
            public ulong A;
            public uint B;
        }

        [Test, Ignore("TODO")]
        public void T1()
        {
            using(var db=new LevelDb("test.leveldb", LevelDbOptions.LdoCreateIfMissing))
            {
                var key = new MyKey {A = 123456, B = 32};
            }
        }
    }
}
