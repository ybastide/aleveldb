﻿using System;
using System.Collections.Generic;
using System.Text;
using ALevelDb;
using NUnit.Framework;
using TswBase;

namespace TestLevelDb
{
    [TestFixture]
    public class TestsIter
    {
        readonly LevelDbOptions _options = new LevelDbOptions { CreateIfMissing = true };

        // ReSharper disable InconsistentNaming
        [Test]
        public void t01()
        {
            try
            {
                LevelDb.Destroy("test.leveldb");
            }
            // ReSharper disable RedundantCatchClause
            catch
            {
                throw;
            }
            // ReSharper restore RedundantCatchClause
            using (var db = new LevelDb("test.leveldb", _options))
            {
                var list = new List<int>();
                const int count = 1000;
                for (int i = 0; i < count; i++)
                    list.Add(i);
                G.ListRandomize(list, new Random());

                foreach (var i in list)
                {
                    db.Put(i.ToString("000"), i);
                }

                using (var iter = db.GetIterator())
                {
                    int prev = -1;
                    int j = 0;
                    foreach (var kv in iter)
                    {
                        //string key = Encoding.UTF8.GetString(kv.Key);
                        int value = BitConverter.ToInt32(kv.Value, 0);
                        //if (value < prev)
                        //{
                        //    Console.WriteLine(value);
                        //}
                        //Assert.Greater(value, prev);
                        Assert.AreEqual(prev + 1, value);
                        prev = value;
                        j++;
                    }
                    Assert.AreEqual(count, j);
                }
            }
        }

        [Test]
        public void t02_Nonexisting()
        {
            LevelDb.Destroy("test.leveldb");
            using (var db = new LevelDb("test.leveldb", _options))
            {
                var list = new List<int>();
                const int count = 1000;
                for (int i = 0; i < count; i++)
                    list.Add(i);

                foreach (var i in list)
                {
                    db.Put(i.ToString("000"), i);
                }

                using (var iter = db.GetIterator("yapas"))
                {
                    Assert.IsFalse(iter.MoveNext());
                }
            }
        }

        [Test]
        public void t03_Desc()
        {
            LevelDb.Destroy("test.leveldb");
            using (var db = new LevelDb("test.leveldb", _options))
            {
                var list = new List<int>();
                const int count = 1000;
                for (int i = 0; i < count; i++)
                    list.Add(i);

                foreach (var i in list)
                {
                    db.Put(i.ToString("000"), i);
                }

                int prev = count;
                int j = 0;
                using (var iter = db.GetIterator(false))
                {
                    foreach (var kv in iter)
                    {
                        //string key = Encoding.UTF8.GetString(kv.Key);
                        int value = BitConverter.ToInt32(kv.Value, 0);
                        //if (value < prev)
                        //{
                        //    Console.WriteLine(value);
                        //}
                        //Assert.Greater(value, prev);
                        Assert.AreEqual(prev - 1, value);
                        prev = value;
                        j++;
                    }
                    Assert.AreEqual(count, j);
                }
            }
        }

        [Test]
        public void t04_AsText()
        {
            LevelDb.Destroy("test.leveldb");
            using (var db = new LevelDb("test.leveldb", _options))
            {
                var list = new List<int>();
                const int count = 1000;
                for (int i = 0; i < count; i++)
                    list.Add(i);

                foreach (var i in list)
                {
                    db.Put(i.ToString("000"), i);
                }

                int prev = -1;
                int j = 0;
                using (var iter = db.GetIterator())
                {
                    foreach (var kv in iter.AsStrings)
                    {
                        int value = int.Parse(kv.Key);
                        Assert.AreEqual(prev + 1, value);
                        prev = value;
                        j++;
                    }
                    Assert.AreEqual(count, j);
                }
            }
        }

        [Test]
        public void t05_From32()
        {
            LevelDb.Destroy("test.leveldb");
            using (var db = new LevelDb("test.leveldb", _options))
            {
                var list = new List<int>();
                const int count = 1000;
                for (int i = 0; i < count; i++)
                    list.Add(i);

                foreach (var i in list)
                {
                    db.Put(i.ToString("000"), i);
                }

                using (var iter = db.GetIterator("032"))
                {
                    Assert.IsTrue(iter.MoveNext());
                    var kv = iter.CurrentAsStrings;
                    Assert.AreEqual("032", kv.Key);
                    Assert.AreEqual(4, kv.Value.Length);
                }
            }
        }


        [Test]
        public void t06_Foreach()
        {
            LevelDb.Destroy("test.leveldb");
            using (var db = new LevelDb("test.leveldb", _options))
            {
                var list = new List<int>();
                const int count = 1000;
                for (int i = 0; i < count; i++)
                    list.Add(i);

                foreach (var i in list)
                {
                    db.Put(i.ToString("000"), i);
                }

                int p = 0;
                int prev = 31;
                using (var levelDbIterator = db.GetIterator("032"))
                {
                    foreach (var kv in levelDbIterator)
                    {
                        int n = int.Parse(Encoding.UTF8.GetString(kv.Key));
                        Assert.AreEqual(prev + 1, n);
                        prev = n;
                        p++;
                        //Assert.IsTrue(iter.MoveNext());
                        //var kv = iter.CurrentAsStrings;
                        //Assert.AreEqual("032", kv.Key);
                        Assert.AreEqual(4, kv.Value.Length);
                    }
                }
                Assert.AreEqual(count - 32, p);
            }
        }

        [Test]
        public void t07_ForeachFromStart()
        {
            LevelDb.Destroy("test.leveldb");
            using (var db = new LevelDb("test.leveldb", _options))
            {
                var list = new List<int>();
                const int count = 1000;
                for (int i = 0; i < count; i++)
                    list.Add(i);

                foreach (var i in list)
                {
                    db.Put(i.ToString("000"), i);
                }

                int p = 0;
                int prev = -1;
                using (var levelDbIterator = db.GetIterator())
                {
                    foreach (var kv in levelDbIterator)
                    {
                        int n = int.Parse(Encoding.UTF8.GetString(kv.Key));
                        Assert.AreEqual(prev + 1, n);
                        prev = n;
                        p++;
                        //Assert.IsTrue(iter.MoveNext());
                        //var kv = iter.CurrentAsStrings;
                        //Assert.AreEqual("032", kv.Key);
                        Assert.AreEqual(4, kv.Value.Length);
                    }
                }
                Assert.AreEqual(count, p);
            }
        }

        // ReSharper restore InconsistentNaming
    }
}
