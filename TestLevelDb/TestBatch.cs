﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ALevelDb;
using NUnit.Framework;

namespace TestLevelDb
{
    [TestFixture]
    public class TestBatch
    {
        readonly LevelDbOptions _options = new LevelDbOptions { CreateIfMissing = true };

        // ReSharper disable InconsistentNaming

        [Test]
        public void t01()
        {
            LevelDb.Destroy("test.leveldb");
            using (var db = new LevelDb("test.leveldb", _options))
            {
                var list = new List<int>();
                const int count = 1000;
                for (int i = 0; i < count; i++)
                    list.Add(i);

                using (var batch = new LevelDbWriteBatch())
                {
                    foreach (var i in list)
                    {
                        batch.Put(i.ToString("000"), i);
                    }
                    var iter = db.GetIterator();
                    Assert.IsFalse(iter.MoveNext());
                    db.Write(batch);
                    iter.Close();
                    iter = db.GetIterator();
                    Assert.IsTrue(iter.MoveNext());
                    iter.Close();
                }
            }
        }

        // ReSharper restore InconsistentNaming
    }
}
