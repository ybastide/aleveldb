﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ALevelDb;
using NUnit.Framework;

namespace TestLevelDb
{
    [TestFixture]
    public class TestSnapshot
    {
        readonly LevelDbOptions _options = new LevelDbOptions { CreateIfMissing = true };

        // ReSharper disable InconsistentNaming

        [Test]
        public void t01()
        {
            LevelDb.Destroy("test.leveldb");
            using (var db = new LevelDb("test.leveldb", _options))
            {
                var list = new List<int>();
                const int count = 1000;
                for (int i = 0; i < count; i++)
                    db.Put(i.ToString("000"), i);

                var snap = db.GetSnapshot();
                using (var iter = db.GetIterator((byte[])null, true, new LevelDbReadOptions { Snapshot = snap }))
                {
                    for (int i = 0; i < count; i++)
                        db.Delete(i.ToString("000"));
                    int prev = -1;
                    int j = 0;
                    foreach (var kv in iter)
                    {
                        int value = BitConverter.ToInt32(kv.Value, 0);
                        Assert.AreEqual(prev + 1, value);
                        prev = value;
                        j++;
                    }
                    Assert.AreEqual(count, j);

                }
                db.ReleaseSnapshot(snap);
            }

        }

        // ReSharper restore InconsistentNaming
    }
}
