﻿using System;
using System.Collections.Generic;
using System.Text;
using ALevelDb;
using NUnit.Framework;
using TswBase;

namespace TestLevelDb
{
    [TestFixture]
    public class TestsPutGetDelete
    {
        // ReSharper disable InconsistentNaming
        [Test]
        public void t01_Create()
        {
            var options = new LevelDbOptions { CreateIfMissing = true };
            using (new LevelDb("test.leveldb", options))
            {
            }
        }

        [Test]
        public void t02_PutGet01()
        {
            var options = new LevelDbOptions { CreateIfMissing = true };
            using (var db = new LevelDb("test.leveldb", options))
            {
                db.Put("int32", 32);
                int res;
                bool rc = db.TryGet("int32", out res);
                Assert.IsTrue(rc);
                Assert.AreEqual(32, res);
            }
        }

        //doh!
        //[Test]
        //public void t02_PutGet02BadValue()
        //{
        //    var options = new LevelDbOptions { CreateIfMissing = true };
        //    using (var db = new LevelDb("test.leveldb", options))
        //    {
        //        db.Put("a", 32);
        //        int res;
        //        bool rc= db.TryGet("a", out res);
        //        Assert.IsTrue(rc);
        //        Assert.AreEqual(3, res);
        //    }
        //}

        [Test]
        public void t02_PutGet03DoesntExist()
        {
            var options = new LevelDbOptions { CreateIfMissing = true };
            using (var db = new LevelDb("test.leveldb", options))
            {
                int res;
                bool rc = db.TryGet("iExistNot", out res);
                Assert.IsFalse(rc);
            }
        }

        [Test, ExpectedException(typeof(LevelDbException))]
        public void t02_PutGet04BadType()
        {
            var options = new LevelDbOptions { CreateIfMissing = true };
            using (var db = new LevelDb("test.leveldb", options))
            {
                db.Put("!int32", "hiiiiiiiiiiiiiiiiiiiii !");
                int res;
                bool rc = db.TryGet("!int32", out res);
                Assert.IsTrue(rc);
                Assert.AreEqual(32, res);
            }
        }

        [Test]
        public void t02_PutGetBytes01()
        {
            var options = new LevelDbOptions { CreateIfMissing = true };
            using (var db = new LevelDb("test.leveldb", options))
            {
                const string value = "hiiiiiiiiiiiiiiiiiiiii !";
                const string key = "!int32";
                db.Put(Encoding.UTF8.GetBytes(key), Encoding.UTF8.GetBytes(value));
                var res = new byte[value.Length];
                int len;
                bool rc = db.TryGet(key, ref res, 0, out len);
                Assert.IsTrue(rc);
                Assert.AreEqual(Encoding.UTF8.GetBytes(value), res);
            }
        }

        [Test]
        public void t02_PutGetBytes02()
        {
            var options = new LevelDbOptions { CreateIfMissing = true };
            using (var db = new LevelDb("test.leveldb", options))
            {
                const string value = "hiiiiiiiiiiiiiiiiiiiii !";
                const string key = "!int32";
                db.Put(key, value);
                var res = new byte[value.Length + 1];
                int len;
                bool rc = db.TryGet(key, ref res, 1, out len);
                Assert.IsTrue(rc);
                var expected = new byte[value.Length + 1];
                Encoding.UTF8.GetBytes(value, 0, value.Length, expected, 1);
                Assert.AreEqual(expected, res);
            }
        }

        [Test]
        public void t02_PutGetBytes03()
        {
            var options = new LevelDbOptions { CreateIfMissing = true };
            using (var db = new LevelDb("test.leveldb", options))
            {
                const string value = "hiiiiiiiiiiiiiiiiiiiii !";
                const string key = "!int32";
                db.Put(key, value);
                byte[] res = null;
                int len;
                bool rc = db.TryGet(key, ref res, 0, out len);
                Assert.IsTrue(rc);
                Assert.AreEqual(Encoding.UTF8.GetBytes(value), res);
            }
        }

        [Test]
        public void t03_PutGetString()
        {
            var options = new LevelDbOptions { CreateIfMissing = true };
            using (var db = new LevelDb("test.leveldb", options))
            {
                const string value = "hiiiiiiiiiiiiiiiiiiiii !";
                const string key = "!int32";
                db.Put(key, value);
                string res;
                bool rc = db.TryGet(key, out res);
                Assert.IsTrue(rc);
                Assert.AreEqual(value, res);
            }
        }

        [Test]
        public void t04_PutDelete01()
        {
            var options = new LevelDbOptions { CreateIfMissing = true };
            using (var db = new LevelDb("test.leveldb", options))
            {
                const string value = "hiiiiiiiiiiiiiiiiiiiii !";
                const string key = "!int32";
                db.Put(key, value);
                string res;
                bool rc = db.TryGet(key, out res);
                Assert.IsTrue(rc);
                db.Delete(key);
                rc = db.TryGet(key, out res);
                Assert.IsFalse(rc);
            }

        }

        [Test]
        public void t05_1000()
        {
            var options = new LevelDbOptions { CreateIfMissing = true };
            using (var db = new LevelDb("test.leveldb", options))
            {
                var list = new List<int>();
                for (int i = 0; i < 1000; i++)
                    list.Add(i);
                G.ListRandomize(list, new Random());

                foreach (var i in list)
                {
                    db.Put(i.ToString("000"), i);
                }
            }
        }

        // ReSharper restore InconsistentNaming
    }
}
