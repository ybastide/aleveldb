// Copyright (c) 2012-2013 The ALevelDb Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file. See the AUTHORS file for names of contributors.

using System;

namespace ALevelDb
{
    /// <summary>
    /// Exception LevelDb : contient un message.
    /// </summary>
    [Serializable]
    public class LevelDbException : Exception
    {
        /// <summary>
        /// Cr�e une exception.
        /// </summary>
        /// <param name="s"></param>
        public LevelDbException(string s)
            : base(s)
        {
        }
    }
}