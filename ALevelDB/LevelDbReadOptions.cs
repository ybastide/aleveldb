using System;

namespace ALevelDb
{
    /// <summary>
    /// Options de lecture.
    /// </summary>
    public class LevelDbReadOptions
    {
        // Copyright (c) 2012-2013 The ALevelDb Authors. All rights reserved.
        // Use of this source code is governed by a BSD-style license that can be
        // found in the LICENSE file. See the AUTHORS file for names of contributors.

        private bool _fillCache = true;

 		/// <summary>
        /// Common use : do not fill the cache.
        /// </summary>
        public static  LevelDbReadOptions WithoutCache = new LevelDbReadOptions {_fillCache = false};

        /// <summary>
        /// Common use : DO fill the cache while reading (default!).
        /// </summary>
        public static LevelDbReadOptions WithCache = new LevelDbReadOptions { _fillCache = true };

        /// <summary>
        /// Default options: fill the read cache.
        /// </summary>
        public static LevelDbReadOptions Default = new LevelDbReadOptions();

        /// <summary>
        /// If true, all data read from underlying storage will be
        /// verified against corresponding checksums.
        /// Default: false
        /// </summary>
        public bool VerifyChecksums { get; set; }

        /// <summary>
        /// Should the data read for this iteration be cached in memory?
        /// Callers may wish to set this field to false for bulk scans.
        /// Default: true
        /// </summary>
        public bool FillCache
        {
            get { return _fillCache; }
            set { _fillCache = value; }
        }

        /// <summary>
        /// Snapshot ?
        /// </summary>
        public LevelDbSnapshot Snapshot { get; set; }

        /// <summary>
        /// Pointeur vers les options. � d�truire par l'appellant.
        /// </summary>
        /// <returns></returns>
        internal IntPtr GetPointer()
        {
            //if (!VerifyChecksums && FillCache && Snapshot.Ptr == IntPtr.Zero)
            //    return defLevelDbReadOptions;
            IntPtr ptr = UnsafeNativeMethods.leveldb_readoptions_create();
            if (VerifyChecksums)
                UnsafeNativeMethods.leveldb_readoptions_set_verify_checksums(ptr, 1);
            if (!FillCache)
                UnsafeNativeMethods.leveldb_readoptions_set_fill_cache(ptr, 0);
            if (Snapshot.Ptr != IntPtr.Zero)
                UnsafeNativeMethods.leveldb_readoptions_set_snapshot(ptr, Snapshot.Ptr);
            return ptr;
        }

        /// <summary>
        /// Vrai si pointeur statique.
        /// </summary>
        /// <param name="readOption"></param>
        /// <returns></returns>
        internal static bool IsMine(IntPtr readOption)
        {
            return false;
        }
    }
}
