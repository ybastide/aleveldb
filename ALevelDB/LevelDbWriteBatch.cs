﻿// Copyright (c) 2012-2013 The ALevelDb Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file. See the AUTHORS file for names of contributors.

using System;
using System.Text;

namespace ALevelDb
{
    /// <summary>
    /// Batch d'écritures.
    /// </summary>
    public class LevelDbWriteBatch : IDisposable
    {
        internal IntPtr Batch;

        /// <summary>
        /// Crée un <see cref="LevelDbWriteBatch"/>.
        /// </summary>
        public LevelDbWriteBatch()
        {
            Batch = UnsafeNativeMethods.leveldb_writebatch_create();
        }

        /// <summary>
        /// Enregistre (key, value).
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Put(byte[] key, byte[] value)
        {
            UnsafeNativeMethods.leveldb_writebatch_put(Batch, key, (IntPtr)key.Length, value, (IntPtr)value.Length);
        }

        /// <summary>
        /// Enregistre (key, value).
        /// </summary>
        /// <param name="kbuf"></param>
        /// <param name="klen"></param>
        /// <param name="vbuf"></param>
        /// <param name="vlen"></param>
        public void Put(byte[] kbuf, int klen, byte[] vbuf, int vlen)
        {
            UnsafeNativeMethods.leveldb_writebatch_put(Batch, kbuf, (IntPtr)klen, vbuf, (IntPtr)vlen);
        }

        /// <summary>
        /// Enregistre (key, value).
        /// </summary>
        /// <param name="key"></param>
        /// <param name="kstart"></param>
        /// <param name="klen"></param>
        /// <param name="val"></param>
        /// <param name="vstart"></param>
        /// <param name="vlen"></param>
        public void Put(byte[] key, int kstart, int klen, byte[] val, int vstart, int vlen)
        {
            unsafe
            {
                fixed (byte* pk = &key[kstart], pv = &val[vstart])
                    UnsafeNativeMethods.leveldb_writebatch_put(Batch, (IntPtr)pk, (IntPtr)klen,
                        (IntPtr)pv, (IntPtr)vlen);
            }
        }

        /// <summary>
        /// Enregistre (key, value).
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Put(string key, int value)
        {
            byte[] bkey = Encoding.UTF8.GetBytes(key);
            byte[] bval = BitConverter.GetBytes(value);
            Put(bkey, bval);
        }

        /// <summary>
        /// Enregistre (key, value).
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Put(string key, string value)
        {
            byte[] bkey = Encoding.UTF8.GetBytes(key);
            byte[] bval = Encoding.UTF8.GetBytes(value);
            Put(bkey, bval);
        }

        /// <summary>
        /// Efface un élément.
        /// </summary>
        /// <param name="key"></param>
        public void Delete(string key)
        {
            byte[] bkey = Encoding.UTF8.GetBytes(key);
            Delete(bkey);
        }

        /// <summary>
        /// Efface un élément.
        /// </summary>
        /// <param name="key"></param>
        public void Delete(byte[] key)
        {
            UnsafeNativeMethods.leveldb_writebatch_delete(Batch, key, (IntPtr)key.Length);
        }

        /// <summary>
        /// Efface un élément.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="klen"></param>
        public void Delete(byte[] key, int klen)
        {
            UnsafeNativeMethods.leveldb_writebatch_delete(Batch, key, (IntPtr)klen);
        }


        /// <summary>
        /// Efface les modifications contenues dans le batch.
        /// </summary>
        public void Clear()
        {
            UnsafeNativeMethods.leveldb_writebatch_clear(Batch);
        }

        /// <summary>
        /// Libère les ressources (n'écrit pas).
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Libère les ressources (n'écrit pas).
        /// </summary>
        public void Close()
        {
            Dispose();
        }

        /// <summary>
        /// Finaliseur.
        /// </summary>
        ~LevelDbWriteBatch()
        {
            Dispose(false);
        }

        // ReSharper disable UnusedParameter.Local
        private void Dispose(bool disposing)
        // ReSharper restore UnusedParameter.Local
        {
            if (Batch != IntPtr.Zero)
                UnsafeNativeMethods.leveldb_writebatch_destroy(Batch);
            //if(disposing)
            {
                Batch = IntPtr.Zero;
            }
        }
    }
}
