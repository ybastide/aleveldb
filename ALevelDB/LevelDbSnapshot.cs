// Copyright (c) 2012-2013 The ALevelDb Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file. See the AUTHORS file for names of contributors.

using System;

namespace ALevelDb
{
    /// <summary>
    /// Snapshot.
    /// </summary>
    public struct LevelDbSnapshot
    {
        internal IntPtr Ptr;

        internal LevelDbSnapshot(IntPtr db)
        {
            Ptr = UnsafeNativeMethods.leveldb_create_snapshot(db);
        }

        internal void Release(IntPtr db)
        {
            UnsafeNativeMethods.leveldb_release_snapshot(db, Ptr);
        }
    }
}