// Copyright (c) 2012-2013 The ALevelDb Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file. See the AUTHORS file for names of contributors.

using System;

namespace ALevelDb
{
    /// <summary>
    /// Comparateur.
    /// </summary>
    public abstract class LevelDbComparison : IDisposable
    {
        private IntPtr _comparison;
        /// <summary>
        /// �tat, utilis� comme on veut
        /// </summary>
        private IntPtr _state;

        /// <summary>
        /// �tat, utilis� comme on veut
        /// </summary>
        public IntPtr State
        {
            get { return _state; }
            set { _state = value; }
        }

        internal IntPtr GetPointer()
        {
            _comparison = UnsafeNativeMethods.leveldb_comparator_create(_state, Destructor, Comparator, Name);
            return _comparison;
        }

        internal void DestroyPointer()
        {
            if (_comparison != IntPtr.Zero)
            {
                UnsafeNativeMethods.leveldb_comparator_destroy(_comparison);
                _comparison = IntPtr.Zero;
            }
        }

        /// <summary>
        /// Appelle DestroyPointer.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Appelle DestroyPointer.
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            DestroyPointer();
        }

        /// <summary>
        /// 
        /// </summary>
        ~LevelDbComparison()
        {
            Dispose(false);
        }


        /// <summary>
        /// Nom du comparateur.
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        protected abstract byte[] Name(IntPtr state);

        // FIXME faire 2 versions : int32 et int64 ?
        /// <summary>
        /// Comparateur.
        /// </summary>
        /// <param name="state"></param>
        /// <param name="a"></param>
        /// <param name="alen"></param>
        /// <param name="b"></param>
        /// <param name="blen"></param>
        /// <returns></returns>
        protected abstract long Comparator(IntPtr state, IntPtr a, IntPtr alen, IntPtr b, IntPtr blen);

        /// <summary>
        /// Destructeur.
        /// </summary>
        /// <param name="state"></param>
        protected abstract void Destructor(IntPtr state);
    }
}