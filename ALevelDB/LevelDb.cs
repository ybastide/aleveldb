﻿// Copyright (c) 2012-2013 The ALevelDb Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file. See the AUTHORS file for names of contributors.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace ALevelDb
{
    /// <summary>
    /// BD LevelDb.
    /// </summary>
    public class LevelDb : ILevelDb
    {
        #region Delegates

        /// <summary>
        /// Comparateur.
        /// </summary>
        /// <param name="state"></param>
        /// <param name="a"></param>
        /// <param name="alen"></param>
        /// <param name="b"></param>
        /// <param name="blen"></param>
        /// <returns></returns>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate long Comparator(IntPtr state,
                                        IntPtr a, IntPtr alen,
                                        IntPtr b, IntPtr blen);

        /// <summary>
        /// Destructeur du comparateur.
        /// </summary>
        /// <param name="state"></param>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void ComparatorDestructor(IntPtr state);

        /// <summary>
        /// Nom du comparateur.
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate byte[] ComparatorName(IntPtr state);

        /// <summary>
        /// Signale la destruction du Writebatch.
        /// </summary>
        /// <param name="state"></param>
        /// <param name="key"></param>
        /// <param name="keylen"></param>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void WritebatchIterateDeleted(IntPtr state, byte[] key, IntPtr keylen);

        /// <summary>
        /// Put du writeback.
        /// </summary>
        /// <param name="state"></param>
        /// <param name="key"></param>
        /// <param name="keylen"></param>
        /// <param name="val"></param>
        /// <param name="vallen"></param>
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void WritebatchIteratePut(IntPtr state,
                                                  byte[] key, IntPtr keylen,
                                                  byte[] val, IntPtr vallen);

        #endregion

        #region CompressionMode enum

        /// <summary>
        /// Mode de compression (<see cref="Snappy"/> par défaut).
        /// </summary>
        public enum CompressionMode
        {
            /// <summary>
            /// Pas de compression.
            /// </summary>
            None = 0,

            /// <summary>
            /// Snappy.
            /// </summary>
            Snappy = 1,
        }

        #endregion

        /// <summary>
        /// Options, pour libérer le comparateur éventuel.
        /// </summary>
        private readonly LevelDbOptions _options;

        /// <summary>
        /// Handle de la BD.
        /// </summary>
        private IntPtr _db;

        private IntPtr _writeOption;
        private IntPtr _readOption;
        private bool _sync;
        internal static IntPtr DefaultReadOptions;

        private static byte[] _temp = new byte[128];

        /// <summary>
        /// Mode synchro (faux par défaut).
        /// </summary>
        public bool Sync
        {
            get { return _sync; }
            set
            {
                if (_sync != value)
                {
                    _sync = value;
                    UnsafeNativeMethods.leveldb_writeoptions_set_sync(_writeOption, (byte)(_sync ? 1 : 0));
                }
            }
        }

        static LevelDb()
        {
            DefaultReadOptions = UnsafeNativeMethods.leveldb_readoptions_create();
        }

        /// <summary>
        /// Constructeur.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="options"></param>
        public LevelDb(string name, LevelDbOptions options)
            : this(name, options, LevelDbReadOptions.Default, LevelDbWriteOptions.Default)
        {
        }

        /// <summary>
        /// Constructeur avec options lecture/écriture.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="options"></param>
        /// <param name="readOptions"></param>
        /// <param name="writeOptions"></param>
        public LevelDb(string name, LevelDbOptions options, LevelDbReadOptions readOptions, LevelDbWriteOptions writeOptions)
        {
            IntPtr pOptions = options.GetPpointer();
            byte[] sName = ToByteArray(name);
            IntPtr errptr = IntPtr.Zero;
            IntPtr db = UnsafeNativeMethods.leveldb_open(pOptions, sName, ref errptr);
            options.DestroyPointer();
            ThrowOnError(errptr);
            _db = db;
            _options = options;
            _readOption = readOptions.GetPointer();
            _writeOption = writeOptions.GetPointer();
        }

        #region IDisposable Members

        /// <summary>
        /// Libère les ressources.
        /// </summary>
        /// <filterpriority>2</filterpriority>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        /// <summary>
        /// Vérifie si erreur ; la throw.
        /// </summary>
        /// <param name="errptr"></param>
        private static void ThrowOnError(IntPtr errptr)
        {
            if (errptr != IntPtr.Zero)
            {
                string s = Marshal.PtrToStringAnsi(errptr);
                if (!String.IsNullOrEmpty(s))
                {
                    UnsafeNativeMethods.leveldb_free(errptr);
                    throw new LevelDbException(s);
                }
            }
        }

        #region auto_writebatch

        private LevelDbWriteBatch _writeBatch;

        /// <summary>
        /// Vrai si <see cref="WriteBatchCreate"/> a été appelé.
        /// </summary>
        public bool HaveWriteBatch { get { return _writeBatch != null; } }

        /// <summary>
        /// Create a new batch.
        /// </summary>
        public void WriteBatchCreate()
        {
            if (_writeBatch != null)
                throw new Exception("write batch already created");

            _writeBatch = new LevelDbWriteBatch();
        }

        /// <summary>
        /// Commit/cancel and close.
        /// </summary>
        /// <param name="commit"> </param>
        /// <param name="writeOptions"></param>
        public void WriteBatchClose(bool commit = false, LevelDbWriteOptions writeOptions = null)
        {
            if (commit)
            {
                if (writeOptions != null)
                    Write(_writeBatch, writeOptions);
                else
                    Write(_writeBatch);
            }

            _writeBatch.Clear();

            _writeBatch.Dispose();

            _writeBatch = null;
        }

        /// <summary>
        /// Cancel, do not close.
        /// </summary>
        public void WriteBatchCancel()
        {
            _writeBatch.Clear();
        }

        /// <summary>
        /// Commit, do not close.
        /// </summary>
        /// <param name="writeOptions"></param>
        public void WriteBatchCommit(LevelDbWriteOptions writeOptions = null)
        {
            if (writeOptions == null)
                Write(_writeBatch);
            else
                Write(_writeBatch, writeOptions);

            _writeBatch.Clear();
        }

        #endregion

        /// <summary>
        /// Renvoie la chaîne avec un \0 final.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        private static byte[] ToByteArray(string s)
        {
            return ToByteArray(s, ref _temp);
        }

        /// <summary>
        /// Renvoie la chaîne avec un \0 final.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="buf"> </param>
        /// <returns></returns>
        private static byte[] ToByteArray(string s, ref byte[] buf)
        {
            int byteCount = Encoding.UTF8.GetByteCount(s);
            if (byteCount + 1 > buf.Length)
                buf = new byte[byteCount + 1];
            Encoding.UTF8.GetBytes(s, 0, s.Length, buf, 0);
            buf[byteCount] = 0;
            return buf;
        }

        /// <summary>
        /// Synonyme de <see cref="Dispose()"/>.
        /// </summary>
        public void Close()
        {
            Dispose();
        }

        /// <summary>
        /// Destructeur.
        /// </summary>
        ~LevelDb()
        {
            Dispose(false);
        }

        // ReSharper disable UnusedParameter.Local
        private void Dispose(bool disposing)
        // ReSharper restore UnusedParameter.Local
        {
            if (_writeBatch != null)
            {
                _writeBatch.Clear();
                _writeBatch.Close();
            }

            if (_db != IntPtr.Zero)
            {
                UnsafeNativeMethods.leveldb_close(_db);
                _options.Dispose();
            }
            if (_readOption != IntPtr.Zero && !LevelDbReadOptions.IsMine(_readOption))
                UnsafeNativeMethods.leveldb_readoptions_destroy(_readOption);
            if (_writeOption != IntPtr.Zero && !LevelDbWriteOptions.IsMine(_writeOption))
                UnsafeNativeMethods.leveldb_writeoptions_destroy(_writeOption);
            //if (disposing)
            {
                _db = IntPtr.Zero;
                _readOption = IntPtr.Zero;
                _writeOption = IntPtr.Zero;
                _writeBatch = null;
            }
        }

        #region get

        private IntPtr Get(byte[] key, out IntPtr vallen)
        {
            IntPtr errptr = IntPtr.Zero;
            IntPtr ptr;
            unsafe
            {
                fixed (byte* pk = key)
                {
                    ptr = UnsafeNativeMethods.leveldb_get(_db, _readOption, (IntPtr)pk, (IntPtr)key.Length, out vallen,
                                                  ref errptr);
                }
            }

            if (errptr != IntPtr.Zero)
                ThrowOnError(errptr);
            return ptr;
        }

        private IntPtr Get(byte[] key, int klen, out IntPtr vallen)
        {
            IntPtr errptr = IntPtr.Zero;
            IntPtr ptr;

            unsafe
            {
                fixed (byte* pk = key)
                {
                    ptr = UnsafeNativeMethods.leveldb_get(_db, _readOption, (IntPtr)pk, (IntPtr)klen, out vallen,
                                                  ref errptr);
                }
            }

            if (errptr != IntPtr.Zero)
                ThrowOnError(errptr);
            return ptr;
        }


        /// <summary>
        /// Recherche la valeur correspondant à la clé. Si <paramref name="buffer"/> null, l'alloue. (fixme: dupliquer pour int[] etc)
        /// </summary>
        /// <param name="key"></param>
        /// <param name="buffer"></param>
        /// <param name="startIndex"></param>
        /// <param name="len"></param>
        /// <returns></returns>
        public bool TryGet(string key, ref byte[] buffer, int startIndex, out int len)
        {
            byte[] bkey = Encoding.UTF8.GetBytes(key);
            IntPtr vallen;
            IntPtr ptr = Get(bkey, out vallen);
            if (ptr == IntPtr.Zero)
            {
                len = 0;
                return false;
            }
            len = vallen.ToInt32();
            if (buffer == null || buffer.Length < len)
                buffer = new byte[len];
            Marshal.Copy(ptr, buffer, startIndex, len);
            UnsafeNativeMethods.leveldb_free(ptr);
            return true;
        }

        /// <summary>
        /// Recherche la valeur correspondant à la clé. Si <paramref name="buffer"/> null, l'alloue. (fixme: dupliquer pour int[] etc)
        /// </summary>
        /// <param name="key"></param>
        /// <param name="klen"></param>
        /// <param name="buffer"></param>
        /// <param name="startIndex"></param>
        /// <param name="len"></param>
        /// <returns></returns>
        public bool TryGet(byte[] key, int klen, ref byte[] buffer, int startIndex, out int len)
        {
            IntPtr vallen;
            IntPtr ptr = Get(key, klen, out vallen);
            if (ptr == IntPtr.Zero)
            {
                len = 0;
                return false;
            }
            len = vallen.ToInt32();
            if (buffer == null || buffer.Length < len)
                buffer = new byte[len];
            Marshal.Copy(ptr, buffer, startIndex, len);
            UnsafeNativeMethods.leveldb_free(ptr);
            return true;
        }


        /// <summary>
        /// Recherche la valeur correspondant à la clé.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="s"></param>
        /// <returns></returns>
        public bool TryGet(string key, out string s)
        {
            byte[] buffer = null;
            int len;
            if (TryGet(key, ref buffer, 0, out len))
            {
                s = Encoding.UTF8.GetString(buffer, 0, len);
                return true;
            }
            s = string.Empty;
            return false;
        }

        /// <summary>
        /// Recherche la valeur correspondant à la clé.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="i"></param>
        /// <returns></returns>
        public unsafe bool TryGet(string key, out int i)
        {
            byte[] bkey = Encoding.UTF8.GetBytes(key);
            IntPtr vallen;
            IntPtr ptr = Get(bkey, out vallen);
            if (ptr == IntPtr.Zero)
            {
                i = default(int);
                return false;
            }
            if (vallen.ToInt32() != 4)
                throw new LevelDbException("Not an Int32 (length=" + vallen + ") key ='" + key + "'");
            i = *(int*)ptr;
            UnsafeNativeMethods.leveldb_free(ptr);
            return true;
        }

        #endregion

        #region put

        /// <summary>
        /// Enregistre (key, value).
        /// bypass batch if any.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="keylen"> </param>
        /// <param name="value"></param>
        /// <param name="vallen"> </param>
        private void Put_no_batch(byte[] key, int keylen, byte[] value, int vallen)
        {
            IntPtr errptr = IntPtr.Zero;

            unsafe
            {
                fixed (byte* pk = key, pv = value)
                    UnsafeNativeMethods.leveldb_put(_db, _writeOption, (IntPtr)pk, (IntPtr)keylen,
                                            (IntPtr)pv, (IntPtr)vallen, ref errptr);
            }

            if (errptr != IntPtr.Zero)
                ThrowOnError(errptr);
        }


        /// <summary>
        /// Enregistre (key, value).
        /// </summary>
        /// <param name="key"></param>
        /// <param name="keyLength"> </param>
        /// <param name="value"></param>
        /// <param name="keyOffset"> </param>
        /// <param name="valOffset"> </param>
        /// <param name="valLength"> </param>
        public void Put(byte[] key, int keyOffset, int keyLength, byte[] value, int valOffset, int valLength)
        {
            if (_writeBatch != null)
            {
                _writeBatch.Put(key, keyOffset, keyLength, value, valOffset, valLength);
            }
            else
            {
                IntPtr errptr = IntPtr.Zero;
                unsafe
                {
                    fixed (byte* pk = &key[keyOffset], pv = &value[valOffset])
                        UnsafeNativeMethods.leveldb_put(_db, _writeOption, (IntPtr)pk, (IntPtr)keyLength, (IntPtr)pv,
                                                (IntPtr)valLength, ref errptr);
                }
                if (errptr != IntPtr.Zero)
                    ThrowOnError(errptr);
            }
        }

        /// <summary>
        /// Enregistre (key, value)
        /// </summary>
        /// <param name="key"></param>
        /// <param name="klen"></param>
        /// <param name="value"></param>
        /// <param name="vlen"></param>
        public void Put(byte[] key, int klen, byte[] value, int vlen)
        {
            if (_writeBatch != null)
                _writeBatch.Put(key, klen, value, vlen);
            else
                Put_no_batch(key, klen, value, vlen);
        }

        /// <summary>
        /// Enregistre (key, value).
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Put(byte[] key, byte[] value)
        {
            Put(key, key.Length, value, value.Length);
        }


        /// <summary>
        /// Enregistre (key, value).
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Put(string key, int value)
        {
            byte[] bkey = Encoding.UTF8.GetBytes(key);
            byte[] bval = BitConverter.GetBytes(value);
            Put(bkey, bval);
        }

#if false
        public void Put<TKey, TValue>(ref TKey key/*, int keyLength*/, ref TValue value/*, int valLength*/)
            where TKey : struct
            where TValue : struct
        {
            int keyLength = Marshal.SizeOf(key);
            int valLength = Marshal.SizeOf(value);
            IntPtr errptr = IntPtr.Zero;
            var gkey = GCHandle.Alloc(key, GCHandleType.Pinned);
            try
            {
                var gvalue = GCHandle.Alloc(value, GCHandleType.Pinned);
                try
                {
                    LevelDbBase.leveldb_put(_db, _writeOption, gkey.AddrOfPinnedObject(), (IntPtr)keyLength,
                                            gvalue.AddrOfPinnedObject(), (IntPtr)valLength, ref errptr);
                }
                finally
                {
                    gvalue.Free();
                }
            }
            finally
            {
                gkey.Free();
            }
            ThrowOnError(errptr);
        }

        public void Put<TValue>(string key/*, int keyLength*/, ref TValue value/*, int valLength*/)
            where TValue : struct
        {
            byte[] bkey = Encoding.UTF8.GetBytes(key);
            int keyLength = bkey.Length;
            int valLength = Marshal.SizeOf(value);
            IntPtr errptr = IntPtr.Zero;
            var gvalue = GCHandle.Alloc(value, GCHandleType.Pinned);
            try
            {
                unsafe
                {
                    fixed (byte* pkey = bkey)
                        LevelDbBase.leveldb_put(_db, _writeOption, (IntPtr)pkey, (IntPtr)keyLength,
                                                gvalue.AddrOfPinnedObject(), (IntPtr)valLength, ref errptr);
                }
            }
            finally
            {
                gvalue.Free();
            }
            ThrowOnError(errptr);
        }
#endif

        /// <summary>
        /// Enregistre (key, value).
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Put(string key, string value)
        {
            byte[] bkey = Encoding.UTF8.GetBytes(key);
            byte[] bval = Encoding.UTF8.GetBytes(value);
            Put(bkey, bval);
        }

        /// <summary>
        /// Enregistre (key, value).
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Put(string key, byte[] value)
        {
            byte[] bkey = Encoding.UTF8.GetBytes(key);
            Put(bkey, value);
        }

        /// <summary>
        /// Enregistre (key, value).
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="offset"> </param>
        /// <param name="length"> </param>
        public void Put(string key, byte[] value, int offset, int length)
        {
            byte[] bkey = Encoding.UTF8.GetBytes(key);
            Put(bkey, 0, bkey.Length, value, offset, length);
        }

        #endregion

        #region delete

        /// <summary>
        /// Efface un élément.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="klen"></param>
        public void Delete(byte[] key, int klen)
        {
            if (_writeBatch != null)
            {
                _writeBatch.Delete(key, klen);
            }
            else
            {
                unsafe
                {
                    IntPtr errptr = IntPtr.Zero;
                    fixed (byte* pk = key)
                        UnsafeNativeMethods.leveldb_delete(_db, _writeOption, (IntPtr)pk, (IntPtr)klen, ref errptr);
                    if (errptr != IntPtr.Zero)
                        ThrowOnError(errptr);
                }
            }
        }


        /// <summary>
        /// Efface un élément.
        /// </summary>
        /// <param name="key"></param>
        public void Delete(string key)
        {
            byte[] bkey = Encoding.UTF8.GetBytes(key);
            Delete(bkey);
        }

        /// <summary>
        /// Efface un élément.
        /// </summary>
        /// <param name="key"></param>
        public void Delete(byte[] key)
        {
            Delete(key, key.Length);
        }

        #endregion

        /// <summary>
        /// Écrit le batch dans la bd.
        /// </summary>
        /// <param name="writeBatch"></param>
        /// <param name="options"></param>
        public void Write(LevelDbWriteBatch writeBatch, LevelDbWriteOptions options)
        {
            IntPtr errptr = IntPtr.Zero;
            UnsafeNativeMethods.leveldb_write(_db, options.GetPointer(), writeBatch.Batch, ref errptr);
            if (errptr != IntPtr.Zero)
                ThrowOnError(errptr);
        }

        /// <summary>
        /// Écrit le batch dans la bd. Sync = false.
        /// </summary>
        /// <param name="writeBatch"></param>
        public void Write(LevelDbWriteBatch writeBatch)
        {
            IntPtr errptr = IntPtr.Zero;
            UnsafeNativeMethods.leveldb_write(_db, LevelDbWriteOptions.NoSyncWriteOption, writeBatch.Batch, ref errptr);
            if (errptr != IntPtr.Zero)
                ThrowOnError(errptr);
        }

        /// <summary>
        /// Renvoie un itérateur (à fermer après !)
        /// </summary>
        /// <returns></returns>
        public LevelDbIterator GetIterator()
        {
            return new LevelDbIterator(_db, null, true, new LevelDbReadOptions());
        }

        /// <summary>
        /// Renvoie un itérateur (à fermer après !)
        /// </summary>
        /// <returns></returns>
        /// <param name="startKey"></param>
        public LevelDbIterator GetIterator(byte[] startKey)
        {
            return new LevelDbIterator(_db, startKey, true, new LevelDbReadOptions());
        }

        /// <summary>
        /// Renvoie un itérateur (à fermer après !)
        /// </summary>
        /// <returns></returns>
        /// <param name="startKey"></param>
        public LevelDbIterator GetIterator(string startKey)
        {
            byte[] bKey = Encoding.UTF8.GetBytes(startKey);
            return new LevelDbIterator(_db, bKey, true, new LevelDbReadOptions());
        }

        /// <summary>
        /// Renvoie un itérateur (à fermer après !)
        /// </summary>
        /// <returns></returns>
        /// <param name="startKey"></param>
        /// <param name="asc"></param>
        public LevelDbIterator GetIterator(byte[] startKey, bool asc)
        {
            return new LevelDbIterator(_db, startKey, asc, new LevelDbReadOptions());
        }

        /// <summary>
        /// Renvoie un itérateur (à fermer après !)
        /// </summary>
        /// <returns></returns>
        /// <param name="startKey"></param>
        /// <param name="asc"></param>
        /// <param name="options"></param>
        public LevelDbIterator GetIterator(byte[] startKey, bool asc, LevelDbReadOptions options)
        {
            return new LevelDbIterator(_db, startKey, asc, options);
        }

        /// <summary>
        /// Renvoie un itérateur (à fermer après !)
        /// </summary>
        /// <returns></returns>
        /// <param name="startKey"></param>
        /// <param name="startkeyLen"></param>
        /// <param name="asc"></param>
        /// <param name="options"></param>
        public LevelDbIterator GetIterator(byte[] startKey, int startkeyLen, bool asc, LevelDbReadOptions options)
        {
            if (options == null)
                options = new LevelDbReadOptions();

            return new LevelDbIterator(_db, startKey, startkeyLen, asc, options);
        }

        /// <summary>
        /// Renvoie un itérateur (à fermer après !)
        /// </summary>
        /// <returns></returns>
        /// <param name="startKey"></param>
        /// <param name="asc"></param>
        public LevelDbIterator GetIterator(string startKey, bool asc)
        {
            byte[] bKey = Encoding.UTF8.GetBytes(startKey);
            return new LevelDbIterator(_db, bKey, asc, new LevelDbReadOptions());
        }

        /// <summary>
        /// Renvoie un itérateur (à fermer après !)
        /// </summary>
        /// <returns></returns>
        /// <param name="startKey"></param>
        /// <param name="asc"></param>
        /// <param name="options"></param>
        public LevelDbIterator GetIterator(string startKey, bool asc, LevelDbReadOptions options)
        {
            byte[] bKey = Encoding.UTF8.GetBytes(startKey);
            return new LevelDbIterator(_db, bKey, asc, options);
        }

        /// <summary>
        /// Renvoie un itérateur (à fermer après !)
        /// </summary>
        /// <param name="asc"></param>
        /// <returns></returns>
        public LevelDbIterator GetIterator(bool asc)
        {
            return new LevelDbIterator(_db, null, asc, new LevelDbReadOptions());
        }

        /// <summary>
        /// Compacte toute la bd.
        /// </summary>
        public void Compact()
        {
            UnsafeNativeMethods.leveldb_compact_range(_db, IntPtr.Zero, IntPtr.Zero, IntPtr.Zero, IntPtr.Zero);
        }

        /// <summary>
        /// Compacte la bd entre deux clés (incluses).
        /// </summary>
        public void CompactRange(string beginKey, string endKey)
        {
            byte[] bBeginKey = Encoding.UTF8.GetBytes(beginKey);
            byte[] bEndKey = Encoding.UTF8.GetBytes(endKey);
            unsafe
            {
                fixed (byte* pbk = bBeginKey, pek = bEndKey)
                    UnsafeNativeMethods.leveldb_compact_range(_db, (IntPtr)pbk, (IntPtr)bBeginKey.Length, (IntPtr)pek,
                                                      (IntPtr)bEndKey.Length);
            }
        }

        /// <summary>
        /// Compacte la bd entre deux clés (incluses).
        /// </summary>
        public void CompactRange(byte[] beginKey, byte[] endKey)
        {
            unsafe
            {
                fixed (byte* pbk = beginKey, pek = endKey)
                    UnsafeNativeMethods.leveldb_compact_range(_db, (IntPtr)pbk, (IntPtr)beginKey.Length, (IntPtr)pek,
                                                      (IntPtr)endKey.Length);
            }
        }

        /// <summary>
        /// Compacte la bd entre deux clés (incluses).
        /// </summary>
        public void CompactRange(byte[] beginKey, int beginKeyLength, byte[] endKey, int endKeyLength)
        {
            unsafe
            {
                fixed (byte* pbk = beginKey, pek = endKey)
                    UnsafeNativeMethods.leveldb_compact_range(_db, (IntPtr)pbk, (IntPtr)beginKeyLength, (IntPtr)pek,
                                                      (IntPtr)endKeyLength);
            }
        }

        /// <summary>
        /// Prend une photo. Utiliser en ReadOption d'un itérateur.
        /// </summary>
        /// <returns></returns>
        public LevelDbSnapshot GetSnapshot()
        {
            return new LevelDbSnapshot(_db);
        }

        /// <summary>
        /// Libère la photo.
        /// </summary>
        /// <param name="snapshot"></param>
        public void ReleaseSnapshot(LevelDbSnapshot snapshot)
        {
            snapshot.Release(_db);
        }

        /// <summary>
        /// Efface la BD si elle existe.
        /// </summary>
        /// <param name="name"></param>
        public static void Destroy(string name)
        {
            var options = new LevelDbOptions();
            IntPtr pOptions = options.GetPpointer();
            byte[] sName = ToByteArray(name);
            IntPtr errptr = IntPtr.Zero;
            UnsafeNativeMethods.leveldb_destroy_db(pOptions, sName, ref errptr);
            options.DestroyPointer();
            ThrowOnError(errptr);
        }

        /// <summary>
        /// Efface la BD si elle existe, sans laner d'exception. Renvoie le message d'erreur éventuel.
        /// </summary>
        /// <param name="name"></param>
        public static string DestroyIfExists(string name)
        {
            var options = new LevelDbOptions();
            IntPtr pOptions = options.GetPpointer();
            byte[] sName = ToByteArray(name);
            IntPtr errptr = IntPtr.Zero;
            UnsafeNativeMethods.leveldb_destroy_db(pOptions, sName, ref errptr);
            options.DestroyPointer();
            string s;
            if (errptr != IntPtr.Zero)
            {
                s = Marshal.PtrToStringAnsi(errptr);
                if (!String.IsNullOrEmpty(s))
                {
                    UnsafeNativeMethods.leveldb_free(errptr);
                }
            }
            else
            {
                s = null;
            }

            return s;
        }

        /// <summary>
        /// Tente de réparer la BD.
        /// </summary>
        /// <param name="name"></param>
        public static void Repair(string name)
        {
            var options = new LevelDbOptions();
            IntPtr pOptions = options.GetPpointer();
            byte[] sName = ToByteArray(name);
            IntPtr errptr = IntPtr.Zero;
            UnsafeNativeMethods.leveldb_repair_db(pOptions, sName, ref errptr);
            options.DestroyPointer();
            ThrowOnError(errptr);
        }

        /// <summary>
        /// Renvoie un itérateur sain.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public IEnumerable<KeyValuePair<byte[], byte[]>> GetIterator2(string s)
        {
            var bytes = Encoding.UTF8.GetBytes(s);
            return GetIterator2(bytes, bytes.Length, true, null);
        }

        /// <summary>
        /// Renvoie un itérateur sain.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="length"></param>
        /// <param name="asc"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public IEnumerable<KeyValuePair<byte[], byte[]>> GetIterator2(byte[] key, int length, bool asc, LevelDbReadOptions options)
        {
            return new LevelDbIterator2(_db, key, length, asc, options);
        }

        /// <summary>
        /// Renvoie un itérateur sain.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public IEnumerable<KeyValuePair<byte[], byte[]>> GetIterator2(byte[] key)
        {
            return new LevelDbIterator2(_db, key, key.Length, true, null);
        }

        /// <summary>
        /// Renvoie un itérateur sain.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<KeyValuePair<byte[], byte[]>> GetIterator2()
        {
            return new LevelDbIterator2(_db, null, 0, true, null);
        }
    }
}
