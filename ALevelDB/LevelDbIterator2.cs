﻿// Copyright (c) 2012-2013 The ALevelDb Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file. See the AUTHORS file for names of contributors.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace ALevelDb
{
    internal class LevelDbIterator2 : IEnumerable<KeyValuePair<byte[], byte[]>>
    {
        internal class IterOptions
        {
            public IntPtr Db { get; private set; }
            public byte[] Key { get; private set; }
            public int Length { get; private set; }
            public bool Ascending { get; private set; }
            public LevelDbReadOptions Options { get; private set; }

            public IterOptions(IntPtr db, byte[] key, int length, bool asc, LevelDbReadOptions options)
            {
                Db = db;
                Key = key;
                Length = length;
                Ascending = asc;
                Options = options;        
            }
        }

        private readonly IterOptions _iterOptions;

        internal LevelDbIterator2(IntPtr db, byte[] bytes, int length, bool asc, LevelDbReadOptions options)
        {
            _iterOptions=new IterOptions(db, bytes, length, asc, options);
        }

        public IEnumerator<KeyValuePair<byte[], byte[]>> GetEnumerator()
        {
            return new IterIter(_iterOptions);
        }

        internal class IterIter : IEnumerator<KeyValuePair<byte[], byte[]>>
        {
            private readonly IterOptions _iterOptions;
            private IntPtr _options;
            private IntPtr _iter;
            private bool _start;

            public IterIter(IterOptions iterOptions)
            {
                _iterOptions = iterOptions;
                _options = iterOptions.Options == null ? LevelDb.DefaultReadOptions : iterOptions.Options.GetPointer();
                _iter = UnsafeNativeMethods.leveldb_create_iterator(iterOptions.Db, _options);
                // ne pas détruire LevelDb.DefaultReadOptions
                if (_options == LevelDb.DefaultReadOptions)
                    _options = IntPtr.Zero;
                Reset();
            }

            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            ~IterIter()
            {
                Dispose(false);
            }

            // ReSharper disable UnusedParameter.Local
            private void Dispose(bool disposing)
                // ReSharper restore UnusedParameter.Local
            {
                if (_iter != IntPtr.Zero)
                    UnsafeNativeMethods.leveldb_iter_destroy(_iter);
                if (_options != IntPtr.Zero)
                    UnsafeNativeMethods.leveldb_readoptions_destroy(_options);
                _iter = IntPtr.Zero;
                _options = IntPtr.Zero;
            }

            public bool MoveNext()
            {
                if (_iter == IntPtr.Zero)
                    throw new ObjectDisposedException(GetType().Name);
                if (_start)
                {
                    _start = false;
                }
                else
                {
                    if (_iterOptions.Ascending)
                        UnsafeNativeMethods.leveldb_iter_next(_iter);
                    else
                        UnsafeNativeMethods.leveldb_iter_prev(_iter);
                }
                bool rc = UnsafeNativeMethods.leveldb_iter_valid(_iter) != 0;
                ThrowOnError();
                return rc;
            }

            public void Reset()
            {
                if (_iter == IntPtr.Zero)
                    throw new ObjectDisposedException(GetType().Name);
                if (_iterOptions.Key == null || _iterOptions.Length == 0)
                {
                    if (_iterOptions.Ascending)
                        UnsafeNativeMethods.leveldb_iter_seek_to_first(_iter);
                    else
                        UnsafeNativeMethods.leveldb_iter_seek_to_last(_iter);
                }
                else
                    UnsafeNativeMethods.leveldb_iter_seek(_iter, _iterOptions.Key, (IntPtr)_iterOptions.Length);
                _start = true;
#if DEBUG
                bool rc = UnsafeNativeMethods.leveldb_iter_valid(_iter) != 0;
#endif
                ThrowOnError();
            }

            /// <summary>
            /// Clé courante.
            /// </summary>
            public byte[] CurrentKey
            {
                get
                {
                    IntPtr klen;
                    IntPtr ptr = UnsafeNativeMethods.leveldb_iter_key(_iter, out klen);
                    var key = new byte[klen.ToInt32()];
                    Marshal.Copy(ptr, key, 0, klen.ToInt32());
                    return key;
                }
            }

            /// <summary>
            /// Valeur courante.
            /// </summary>
            public byte[] CurrentValue
            {
                get
                {
                    IntPtr vlen;
                    IntPtr ptr = UnsafeNativeMethods.leveldb_iter_value(_iter, out vlen);
                    var value = new byte[vlen.ToInt32()];
                    Marshal.Copy(ptr, value, 0, vlen.ToInt32());
                    return value;
                }
            }

            public KeyValuePair<byte[], byte[]> Current
            {
                get { return new KeyValuePair<byte[], byte[]>(CurrentKey, CurrentValue); }
            }

            object IEnumerator.Current
            {
                get { return Current; }
            }

            /// <summary>
            /// Vérifie si erreur ; la throw, détruit l'itérateur.
            /// </summary>
            private void ThrowOnError()
            {
                IntPtr errptr = IntPtr.Zero;
                UnsafeNativeMethods.leveldb_iter_get_error(_iter, ref errptr);
                if (errptr == IntPtr.Zero)
                    return;
                string s = Marshal.PtrToStringAnsi(errptr);
                UnsafeNativeMethods.leveldb_free(errptr);
                Dispose();
                throw new LevelDbException(s);
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}