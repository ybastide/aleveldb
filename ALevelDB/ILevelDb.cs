﻿using System;

namespace ALevelDb
{
    public interface ILevelDb : IDisposable
    {
        /// <summary>
        /// Create a new batch.
        /// </summary>
        void WriteBatchCreate();

        /// <summary>
        /// Commit/cancel and close.
        /// </summary>
        /// <param name="commit"> </param>
        /// <param name="writeOptions"></param>
        void WriteBatchClose(bool commit = false, LevelDbWriteOptions writeOptions = null);

        /// <summary>
        /// Cancel, do not close.
        /// </summary>
        void WriteBatchCancel();

        /// <summary>
        /// Commit, do not close.
        /// </summary>
        /// <param name="writeOptions"></param>
        void WriteBatchCommit(LevelDbWriteOptions writeOptions = null);

        /// <summary>
        /// Enregistre (key, value)
        /// </summary>
        /// <param name="key"></param>
        /// <param name="klen"></param>
        /// <param name="value"></param>
        /// <param name="vlen"></param>
        void Put(byte[] key, int klen, byte[] value, int vlen);
    }
}