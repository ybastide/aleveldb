﻿// Copyright (c) 2012-2013 The ALevelDb Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file. See the AUTHORS file for names of contributors.

using System;
using System.Runtime.InteropServices;
using System.Security;

namespace ALevelDb
{
    [SuppressUnmanagedCodeSecurity]
    internal static class UnsafeNativeMethods
    {
        /* FIXME BUG
         * linux x64  : IntPtr est 64 bits, int .Net est 32 bits, int C est 64 bits, size_t est 64 bits (et time_t et ptrdiff_t)
         * windows 64 : IntPtr est 64 bits, int .Net est 32 bits, int C est 32 bits, size_t est 64 bits (et time_t et ptrdiff_t)
         * donc IntPtr <=> size_t
         */

        public static bool Is64;
        public static bool IsUnix;

        public static bool IntC32;

        static UnsafeNativeMethods()
        {
            Is64 = IntPtr.Size == 8;
            IsUnix = ((int)Environment.OSVersion.Platform == 4 || (int)Environment.OSVersion.Platform == 255);
            IntC32 = Is64 && !IsUnix;
        }

        [DllImport("leveldb")]
        internal static extern void leveldb_free(IntPtr ptr);

        /* DB operations */

        [DllImport("leveldb")]
        internal static extern IntPtr leveldb_open(IntPtr options,
                                                   byte[] name,
                                                   ref IntPtr errptr);

        [DllImport("leveldb")]
        internal static extern void leveldb_close(IntPtr db);

        [DllImport("leveldb")]
        internal static extern void leveldb_put(IntPtr db,
                                                IntPtr options,
                                                IntPtr key, IntPtr keylen,
                                                IntPtr val, IntPtr vallen,
                                                ref IntPtr errptr);

        [DllImport("leveldb")]
        internal static extern IntPtr leveldb_get(IntPtr db,
                                                  IntPtr options,
                                                  IntPtr key, IntPtr keylen,
                                                  out IntPtr vallen,
                                                  ref IntPtr errptr);

        [DllImport("leveldb")]
        internal static extern void leveldb_delete(IntPtr db,
                                                   IntPtr options,
                                                   IntPtr key, IntPtr keylen,
                                                   ref IntPtr errptr);

        [DllImport("leveldb")]
        internal static extern void leveldb_write(IntPtr db,
                                                  IntPtr options,
                                                  IntPtr batch,
                                                  ref IntPtr errptr);

        [DllImport("leveldb")]
        internal static extern IntPtr leveldb_create_iterator(IntPtr db,
                                                              IntPtr options);

        // ReSharper disable InconsistentNaming
        [DllImport("leveldb")]
        internal static extern void leveldb_compact_range(IntPtr db,
                                                          IntPtr start_key, IntPtr start_key_len,
                                                          IntPtr limit_key, IntPtr limit_key_len);
        // ReSharper restore InconsistentNaming

        [DllImport("leveldb")]
        internal static extern IntPtr leveldb_create_snapshot(IntPtr db);

        [DllImport("leveldb")]
        internal static extern void leveldb_release_snapshot(IntPtr db,
                                                             IntPtr snapshot);

        [DllImport("leveldb")]
        internal static extern IntPtr leveldb_property_value(IntPtr db,
                                                             byte[] propname);

        [DllImport("leveldb")]
        internal static extern void leveldb_approximate_sizes(IntPtr db,
                                                              Int32 num_ranges,
                                                              [In] IntPtr[] range_start_key, [In] IntPtr[] range_start_key_len,
                                                              [In] IntPtr[] range_limit_key, [In] IntPtr[] range_limit_key_len,
                                                              ulong[] sizes);

        // ReSharper disable InconsistentNaming
        //[DllImport("leveldb", EntryPoint = "leveldb_approximate_sizes")]
        //internal static extern void leveldb_approximate_sizes_32(IntPtr db,
        //                                                         Int32 num_ranges,
        //                                                         IntPtr[] range_start_key, IntPtr[] range_start_key_len,
        //                                                         IntPtr[] range_limit_key, IntPtr[] range_limit_key_len,
        //                                                         ulong[] sizes);

        //[DllImport("leveldb", EntryPoint = "leveldb_approximate_sizes")]
        //internal static extern void leveldb_approximate_sizes_64(IntPtr db,
        //                                                         Int64 num_ranges,
        //                                                         IntPtr[] range_start_key, IntPtr[] range_start_key_len,
        //                                                         IntPtr[] range_limit_key, IntPtr[] range_limit_key_len,
        //                                                         ulong[] sizes);

        // ReSharper restore InconsistentNaming

        /* Management operations */

        [DllImport("leveldb")]
        internal static extern void leveldb_destroy_db(IntPtr options,
                                                       byte[] name,
                                                       ref IntPtr errptr);

        [DllImport("leveldb")]
        internal static extern void leveldb_repair_db(IntPtr options,
                                                      byte[] name,
                                                      ref IntPtr errptr);

        /* Iterator */

        [DllImport("leveldb")]
        internal static extern void leveldb_iter_destroy(IntPtr iter);

        [DllImport("leveldb")]
        internal static extern byte leveldb_iter_valid(IntPtr iter);

        [DllImport("leveldb")]
        internal static extern void leveldb_iter_seek_to_first(IntPtr iter);

        [DllImport("leveldb")]
        internal static extern void leveldb_iter_seek_to_last(IntPtr iter);

        [DllImport("leveldb")]
        internal static extern void leveldb_iter_seek(IntPtr iter, IntPtr k, IntPtr klen);

        [DllImport("leveldb")]
        internal static extern void leveldb_iter_seek(IntPtr iter, byte[] k, IntPtr klen);

        [DllImport("leveldb")]
        internal static extern void leveldb_iter_next(IntPtr iter);

        [DllImport("leveldb")]
        internal static extern void leveldb_iter_prev(IntPtr iter);

        [DllImport("leveldb")]
        internal static extern IntPtr leveldb_iter_key(IntPtr iter, out IntPtr klen);

        [DllImport("leveldb")]
        internal static extern IntPtr leveldb_iter_value(IntPtr iter, out IntPtr vlen);

        [DllImport("leveldb")]
        internal static extern void leveldb_iter_get_error(IntPtr iter, ref IntPtr errptr);

        /* Write batch */

        [DllImport("leveldb")]
        internal static extern IntPtr leveldb_writebatch_create();

        [DllImport("leveldb")]
        internal static extern void leveldb_writebatch_destroy(IntPtr batch);

        [DllImport("leveldb")]
        internal static extern void leveldb_writebatch_clear(IntPtr batch);

        [DllImport("leveldb")]
        internal static extern void leveldb_writebatch_put(
            IntPtr batch,
            IntPtr key, IntPtr keylen,
            IntPtr val, IntPtr vallen);

        [DllImport("leveldb")]
        internal static extern void leveldb_writebatch_delete(IntPtr batch,
                                                              IntPtr key, IntPtr keylen);

        [DllImport("leveldb")]
        internal static extern void leveldb_writebatch_put(
            IntPtr batch,
            byte[] key, IntPtr keylen,
            byte[] val, IntPtr vallen);

        [DllImport("leveldb")]
        internal static extern void leveldb_writebatch_delete(IntPtr batch,
                                                              byte[] key, IntPtr keylen);

        [DllImport("leveldb")]
        internal static extern void leveldb_writebatch_iterate(IntPtr batch,
                                                               IntPtr state,
                                                               LevelDb.WritebatchIteratePut put,
                                                               LevelDb.WritebatchIterateDeleted deleted);

        /* Options */

        [DllImport("leveldb")]
        internal static extern IntPtr leveldb_options_create();

        [DllImport("leveldb")]
        internal static extern void leveldb_options_destroy(IntPtr options);

        [DllImport("leveldb")]
        internal static extern void leveldb_options_set_comparator(IntPtr options,
                                                                   IntPtr comparator);

        [DllImport("leveldb")]
        internal static extern void leveldb_options_set_filter_policy(IntPtr options, IntPtr filterPolicy);

        [DllImport("leveldb")]
        internal static extern void leveldb_options_set_create_if_missing(IntPtr options, byte cim);

        [DllImport("leveldb")]
        internal static extern void leveldb_options_set_error_if_exists(IntPtr options, byte eie);

        [DllImport("leveldb")]
        internal static extern void leveldb_options_set_paranoid_checks(IntPtr options, byte pc);

        [DllImport("leveldb")]
        internal static extern void leveldb_options_set_env(IntPtr options, IntPtr env);

        [DllImport("leveldb")]
        internal static extern void leveldb_options_set_info_log(IntPtr options, IntPtr logger);

        [DllImport("leveldb")]
        internal static extern void leveldb_options_set_write_buffer_size(IntPtr options, IntPtr size); 

        [DllImport("leveldb")]
        internal static extern void leveldb_options_set_max_open_files(IntPtr options, int max);

        //[DllImport("leveldb", EntryPoint = "leveldb_options_set_max_open_files")]
        //internal static extern void leveldb_options_set_max_open_files_32(IntPtr options, Int32 max);

        //[DllImport("leveldb", EntryPoint = "leveldb_options_set_max_open_files")]
        //internal static extern void leveldb_options_set_max_open_files_64(IntPtr options, Int64 max);

        [DllImport("leveldb")]
        internal static extern void leveldb_options_set_cache(IntPtr options, IntPtr cache);

        [DllImport("leveldb")]
        internal static extern void leveldb_options_set_block_size(IntPtr options, IntPtr size);

        [DllImport("leveldb")]
        internal static extern void leveldb_options_set_block_restart_interval(IntPtr options, int interval);

        //[DllImport("leveldb", EntryPoint = "leveldb_options_set_block_restart_interval")]
        //internal static extern void leveldb_options_set_block_restart_interval_32(IntPtr options, Int32 interval);

        //[DllImport("leveldb", EntryPoint = "leveldb_options_set_block_restart_interval")]
        //internal static extern void leveldb_options_set_block_restart_interval_64(IntPtr options, Int64 interval);

        [DllImport("leveldb")]
        internal static extern void leveldb_options_set_compression(IntPtr options, int compression);

        //[DllImport("leveldb", EntryPoint = "leveldb_options_set_compression")]
        //internal static extern void leveldb_options_set_compression32(IntPtr options, Int32 compression);

        //[DllImport("leveldb", EntryPoint = "leveldb_options_set_compression")]
        //internal static extern void leveldb_options_set_compression64(IntPtr options, Int64 compression);


        /* Comparator */

        [DllImport("leveldb")]
        internal static extern IntPtr leveldb_comparator_create(IntPtr state,
                                                                LevelDb.ComparatorDestructor destructor,
                                                                LevelDb.Comparator comparator,
                                                                LevelDb.ComparatorName name);

        [DllImport("leveldb")]
        internal static extern void leveldb_comparator_destroy(IntPtr comparator);

        /* Filter policy */

    //    extern leveldb_filterpolicy_t* leveldb_filterpolicy_create(
    //void* state,
    //void (*destructor)(void*),
    //char* (*create_filter)(
    //    void*,
    //    const char* const* key_array, const size_t* key_length_array,
    //    int num_keys,
    //    size_t* filter_length),
    //unsigned char (*key_may_match)(
    //    void*,
    //    const char* key, size_t length,
    //    const char* filter, size_t filter_length),
    //const char* (*name)(void*));

        [DllImport("leveldb")]
        internal static extern IntPtr leveldb_filterpolicy_create_bloom(IntPtr bitsPerKey);

        [DllImport("leveldb")]
        internal static extern void leveldb_filterpolicy_destroy(IntPtr filterPolicy);

        /* Read options */

        [DllImport("leveldb")]
        internal static extern IntPtr leveldb_readoptions_create();

        [DllImport("leveldb")]
        internal static extern void leveldb_readoptions_destroy(IntPtr options);

        [DllImport("leveldb")]
        internal static extern void leveldb_readoptions_set_verify_checksums(IntPtr options, byte verify);

        [DllImport("leveldb")]
        internal static extern void leveldb_readoptions_set_fill_cache(IntPtr options, byte fill);

        [DllImport("leveldb")]
        internal static extern void leveldb_readoptions_set_snapshot(IntPtr options, IntPtr snapshot);

        /* Write options */

        [DllImport("leveldb")]
        internal static extern IntPtr leveldb_writeoptions_create();

        [DllImport("leveldb")]
        internal static extern void leveldb_writeoptions_destroy(IntPtr options);

        [DllImport("leveldb")]
        internal static extern void leveldb_writeoptions_set_sync(IntPtr options, byte sync);

        /* Cache */

        [DllImport("leveldb")]
        internal static extern IntPtr leveldb_cache_create_lru(IntPtr capacity);

        [DllImport("leveldb")]
        internal static extern void leveldb_cache_destroy(IntPtr cache);

        /* Env */

        [DllImport("leveldb")]
        internal static extern IntPtr leveldb_create_default_env();

        [DllImport("leveldb")]
        internal static extern void leveldb_env_destroy(IntPtr env);
    }
}