﻿// Copyright (c) 2012-2013 The ALevelDb Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file. See the AUTHORS file for names of contributors.

#define SAFER
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace ALevelDb
{
    /// <summary>
    /// Itérateur sur des enregistrements. Utiliser dans un "using" ou appeler <see cref="Close"/>.
    /// </summary>
    public class LevelDbIterator : IEnumerator<KeyValuePair<byte[], byte[]>>, IEnumerable<KeyValuePair<byte[], byte[]>>
    {
        private readonly bool _asc;
        private readonly byte[] _key;
        private readonly int _keyLen;
        private IntPtr _iter;
        private IntPtr _options;
        private bool _start;

        /// <summary>
        /// Constructeur.
        /// </summary>
        /// <param name="db"></param>
        /// <param name="key"></param>
        /// <param name="asc"></param>
        /// <param name="options"></param>
        internal LevelDbIterator(IntPtr db, byte[] key, bool asc, LevelDbReadOptions options)
        {
            _key = key;
            _keyLen = key == null ? 0 : key.Length;
            _asc = asc;
            _options = options.GetPointer();
            _iter = UnsafeNativeMethods.leveldb_create_iterator(db, _options);
            // ne pas détruire LevelDb.DefaultReadOptions
            if (_options == LevelDb.DefaultReadOptions)
                _options = IntPtr.Zero;
            Reset();
        }

        /// <summary>
        /// Constructeur.
        /// </summary>
        /// <param name="db"></param>
        /// <param name="key"></param>
        /// <param name="keyLen"></param>
        /// <param name="asc"></param>
        /// <param name="options"></param>
        internal LevelDbIterator(IntPtr db, byte[] key, int keyLen, bool asc, LevelDbReadOptions options)
        {
            _key = key;
            _keyLen = keyLen;

            _asc = asc;
            _options = options.GetPointer();
            _iter = UnsafeNativeMethods.leveldb_create_iterator(db, _options);
            // ne pas détruire LevelDb.DefaultReadOptions
            if (_options == LevelDb.DefaultReadOptions)
                _options = IntPtr.Zero;
            Reset();
        }

        /// <summary>
        /// Énumérateur.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<KeyValuePair<string, string>> AsStrings
        {
            get
            {
                while (MoveNext())
                    yield return CurrentAsStrings;
            }
        }

        /// <summary>
        /// Valeur courante.
        /// </summary>
        public KeyValuePair<string, string> CurrentAsStrings
        {
            get
            {
                return new KeyValuePair<string, string>(Encoding.UTF8.GetString(CurrentKey),
                                                        Encoding.UTF8.GetString(CurrentValue));
            }
        }

        /// <summary>
        /// Clé courante.
        /// </summary>
        public byte[] CurrentKey
        {
            get
            {
                IntPtr klen;
                IntPtr ptr = UnsafeNativeMethods.leveldb_iter_key(_iter, out klen);
                var key = new byte[klen.ToInt32()];
                Marshal.Copy(ptr, key, 0, klen.ToInt32());
                return key;
            }
        }

        /// <summary>
        /// Valeur courante.
        /// </summary>
        public byte[] CurrentValue
        {
            get
            {
                IntPtr vlen;
                IntPtr ptr = UnsafeNativeMethods.leveldb_iter_value(_iter, out vlen);
                var value = new byte[vlen.ToInt32()];
                Marshal.Copy(ptr, value, 0, vlen.ToInt32());
                return value;
            }
        }

        #region IEnumerable<KeyValuePair<byte[],byte[]>> Members

        /// <summary>
        /// Énumérateur.
        /// </summary>
        /// <returns></returns>
        public IEnumerator<KeyValuePair<byte[], byte[]>> GetEnumerator()
        {
            while (MoveNext())
                yield return Current;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region IEnumerator<KeyValuePair<byte[],byte[]>> Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <filterpriority>2</filterpriority>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        /// <summary>
        /// Élément suivant.
        /// </summary>
        /// <returns></returns>
        public bool MoveNext()
        {
            if (_iter == IntPtr.Zero)
                throw new ObjectDisposedException(GetType().Name);
            if (_start)
            {
                _start = false;
            }
            else
            {
                if (_asc)
                    UnsafeNativeMethods.leveldb_iter_next(_iter);
                else
                    UnsafeNativeMethods.leveldb_iter_prev(_iter);
            }
            bool rc = UnsafeNativeMethods.leveldb_iter_valid(_iter) != 0;
            ThrowOnError();
            return rc;
        }

        /// <summary>
        /// Réinitialise l'itérateur.
        /// </summary>
        public void Reset()
        {
            if (_iter == IntPtr.Zero)
                throw new ObjectDisposedException(GetType().Name);
            if (_key == null || _keyLen == 0)
            {
                if (_asc)
                    UnsafeNativeMethods.leveldb_iter_seek_to_first(_iter);
                else
                    UnsafeNativeMethods.leveldb_iter_seek_to_last(_iter);
            }
            else
                UnsafeNativeMethods.leveldb_iter_seek(_iter, _key, (IntPtr)_keyLen);
            _start = true;
            bool rc = UnsafeNativeMethods.leveldb_iter_valid(_iter) != 0;
            ThrowOnError();
        }

        /// <summary>
        /// (clé, valeur) courants.
        /// </summary>
        public KeyValuePair<byte[], byte[]> Current
        {
            get { return new KeyValuePair<byte[], byte[]>(CurrentKey, CurrentValue); }
        }

        object IEnumerator.Current
        {
            get { return Current; }
        }

        #endregion

        /// <summary>
        /// Synonyme de <see cref="Dispose()"/>.
        /// </summary>
        public void Close()
        {
            Dispose();
        }

        /// <summary>
        /// Destructeur.
        /// </summary>
        ~LevelDbIterator()
        {
            Dispose(false);
        }

        /// <summary>
        /// Détruit les ressources.
        /// </summary>
        /// <param name="disposing"></param>
        // ReSharper disable UnusedParameter.Local
        private void Dispose(bool disposing)
        // ReSharper restore UnusedParameter.Local
        {
            if (_iter != IntPtr.Zero)
                UnsafeNativeMethods.leveldb_iter_destroy(_iter);
            if (_options != IntPtr.Zero)
                UnsafeNativeMethods.leveldb_readoptions_destroy(_options);
            _iter = IntPtr.Zero;
            _options = IntPtr.Zero;
        }

        /// <summary>
        /// Copy current key into buffer, creating/resizing it if needed.
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="len"></param>
        public void CopyCurrentKey(ref byte[] buffer, out int len)
        {
            IntPtr klen;
            IntPtr ptr = UnsafeNativeMethods.leveldb_iter_key(_iter, out klen);

            len = klen.ToInt32();

            if (buffer == null || buffer.Length < len)
                buffer = new byte[len];

            Marshal.Copy(ptr, buffer, 0, len);
        }

        /// <summary>
        /// Copy current value into buffer, creating/resizing it if needed.
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="len"></param>
        public void CopyCurrentValue(ref byte[] buffer, out int len)
        {
            IntPtr vlen;
            IntPtr ptr = UnsafeNativeMethods.leveldb_iter_value(_iter, out vlen);

            len = vlen.ToInt32();

            if (buffer == null || buffer.Length < len)
                buffer = new byte[len];

            Marshal.Copy(ptr, buffer, 0, len);
        }

        /// <summary>
        /// Get the value's length (without copying the value).
        /// </summary>
        public int CurrentValueLength
        {
            get
            {
                IntPtr vlen;
                UnsafeNativeMethods.leveldb_iter_value(_iter, out vlen);

                return vlen.ToInt32();
            }
        }

        /// <summary>
        /// Copy only part of the value !TODO! : need tests (aka : untested)
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="sourceIndex"></param>
        /// <param name="destinationIndex"></param>
        /// <param name="length"></param>
        public unsafe void CopyCurrentValue(byte[] buffer, int sourceIndex, int destinationIndex, int length)
        {
            IntPtr vlen;
            var b = (byte*)UnsafeNativeMethods.leveldb_iter_value(_iter, out vlen);
#if SAFER
            if (sourceIndex > vlen.ToInt32()) throw new IndexOutOfRangeException();
#endif

            b += sourceIndex;

            Marshal.Copy((IntPtr)b, buffer, destinationIndex, length);
        }

        /// <summary>
        /// Get one byte of the key.
        /// </summary>
        /// <param name="pos"></param>
        /// <returns></returns>
        public unsafe byte CurrentKeyGetByte(int pos)
        {
            IntPtr vlen;
            var b = (byte*)UnsafeNativeMethods.leveldb_iter_key(_iter, out vlen);
#if SAFER
            if (pos > vlen.ToInt32()) throw new IndexOutOfRangeException();
#endif

            b += pos;

            return *b;
        }

        /// <summary>
        /// Vérifie si erreur ; la throw, détruit l'itérateur.
        /// </summary>
        private void ThrowOnError()
        {
            IntPtr errptr = IntPtr.Zero;
            UnsafeNativeMethods.leveldb_iter_get_error(_iter, ref errptr);
            if (errptr == IntPtr.Zero)
                return;
            string s = Marshal.PtrToStringAnsi(errptr);
            UnsafeNativeMethods.leveldb_free(errptr);
            Dispose();
            throw new LevelDbException(s);
        }
    }
}
