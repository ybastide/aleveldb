// Copyright (c) 2012-2013 The ALevelDb Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file. See the AUTHORS file for names of contributors.

using System;

namespace ALevelDb
{
    /// <summary>
    /// Options. Ne pas r�utiliser si <see cref="Comparison"/> ou <see cref="CacheSizeMb"/> ou <see cref="BloomFilterPolicy"/> personnalis�.
    /// </summary>
    public class LevelDbOptions
    {
        /// <summary>
        /// Taille par d�faut du buffer (octets).
        /// </summary>
        public const int kDefaultWriteBufferSize = 4 << 20;

        /// <summary>
        /// Taille par d�faut du buffer (Mio).
        /// </summary>
        public const int kDefaultWriteBufferSizeMb = 4;

        /// <summary>
        /// Nombre max de fichiers ouverts par d�faut.
        /// </summary>
        public const int kDefaultMaxOpenFiles = 1000;

        /// <summary>
        /// Taille de bloc par d�faut.
        /// </summary>
        public const int kDefaultBlockSize = 4096;

        /// <summary>
        /// Cf doc :)
        /// </summary>
        public const int kDefaultBlockRestartInterval = 16;

        /// <summary>
        /// Options, with CreateIfMissing.
        /// </summary>
        public static LevelDbOptions LdoCreateIfMissing = new LevelDbOptions { CreateIfMissing = true };

        private IntPtr _cachePointer;
        private IntPtr _filterPointer;

        private int _blockRestartInterval = kDefaultBlockRestartInterval;
        private int _blockSize = kDefaultBlockSize;
        private LevelDb.CompressionMode _compressionMode = LevelDb.CompressionMode.Snappy;
        private int _maxOpenFiles = kDefaultMaxOpenFiles;
        private IntPtr _options;
        private int _writeBufferSize = kDefaultWriteBufferSize;

        /// <summary>
        /// Compareur. D�faut : lexicographique.
        /// </summary>
        public LevelDbComparison Comparison { get; set; }

        /// <summary>
        /// If true, the database will be created if it is missing.
        /// Default: false
        /// </summary>
        public bool CreateIfMissing { get; set; }

        /// <summary>
        /// If true, an error is raised if the database already exists.
        /// Default: false
        /// </summary>
        public bool ErrorIfExists { get; set; }

        /// <summary>
        /// If true, the implementation will do aggressive checking of the
        /// data it is processing and will stop early if it detects any
        /// errors.  This may have unforeseen ramifications: for example, a
        /// corruption of one DB entry may cause a large number of entries to
        /// become unreadable or for the entire DB to become unopenable.
        /// Default: false
        /// </summary>
        public bool ParanoidChecks { get; set; }

        /// <summary>
        /// Amount of data to build up in memory (backed by an unsorted log
        /// on disk) before converting to a sorted on-disk file.
        ///
        /// Larger values increase performance, especially during bulk loads.
        /// Up to two write buffers may be held in memory at the same time,
        /// so you may wish to adjust this parameter to control memory usage.
        /// Also, a larger write buffer will result in a longer recovery time
        /// the next time the database is opened.
        ///
        /// Default: 4MB
        /// </summary>
        public int WriteBufferSize
        {
            get { return _writeBufferSize; }
            set { _writeBufferSize = value; }
        }

        /// <summary>
        /// Amount of data to build up in memory (backed by an unsorted log
        /// on disk) before converting to a sorted on-disk file.
        ///
        /// Larger values increase performance, especially during bulk loads.
        /// Up to two write buffers may be held in memory at the same time,
        /// so you may wish to adjust this parameter to control memory usage.
        /// Also, a larger write buffer will result in a longer recovery time
        /// the next time the database is opened.
        ///
        /// Default: 4MB
        /// </summary>
        public int WriteBufferSizeMb
        {
            get { return _writeBufferSize >> 20; }
            set { _writeBufferSize = value << 20; }
        }

        /// <summary>
        /// Number of open files that can be used by the DB
        /// (one open file per 2MB of working set).
        /// Default: 1000
        /// </summary>
        public int MaxOpenFiles
        {
            get { return _maxOpenFiles; }
            set { _maxOpenFiles = value; }
        }

        /// <summary>
        /// Approximate size of user data packed per block.
        /// This parameter can be changed dynamically.
        /// 
        /// Default: 4K
        /// </summary>
        public int BlockSize
        {
            get { return _blockSize; }
            set { _blockSize = value; }
        }

        /// <summary>
        /// Number of keys between restart points for delta encoding of keys.
        /// This parameter can be changed dynamically.  Most clients should
        /// leave this parameter alone.
        ///
        /// Default: 16
        /// </summary>
        public int BlockRestartInterval
        {
            get { return _blockRestartInterval; }
            set { _blockRestartInterval = value; }
        }

        /// <summary>
        /// Compress blocks using the specified compression algorithm.  This
        /// parameter can be changed dynamically.
        ///
        /// Default: kSnappyCompression
        /// </summary>
        public LevelDb.CompressionMode CompressionMode
        {
            get { return _compressionMode; }
            set { _compressionMode = value; }
        }

        /// <summary>
        /// Fixer la taille du cache LRU.
        /// 
        /// Default: 8MB
        /// </summary>
        public int CacheSizeMb { get; set; }

        /// <summary>
        /// bloom filter with approximately
        /// the specified number of bits per key.  A good value for bits_per_key
        /// is 10, which yields a filter with ~ 1% false positive rate.
        /// 
        /// Default: no filter
        /// </summary>
        public int BloomFilterPolicy { get; set; }

        internal IntPtr GetPpointer()
        {
            _options = UnsafeNativeMethods.leveldb_options_create();
            if (Comparison != null)
                UnsafeNativeMethods.leveldb_options_set_comparator(_options, Comparison.GetPointer());
            if (CreateIfMissing)
                UnsafeNativeMethods.leveldb_options_set_create_if_missing(_options, 1);
            if (ErrorIfExists)
                UnsafeNativeMethods.leveldb_options_set_error_if_exists(_options, 1);
            if (ParanoidChecks)
                UnsafeNativeMethods.leveldb_options_set_paranoid_checks(_options, 1);
            if (WriteBufferSize != kDefaultWriteBufferSize)
                UnsafeNativeMethods.leveldb_options_set_write_buffer_size(_options, (IntPtr)WriteBufferSize);
            if (MaxOpenFiles != kDefaultMaxOpenFiles)
            {
                UnsafeNativeMethods.leveldb_options_set_max_open_files(_options, MaxOpenFiles);
                //if (UnsafeNativeMethods.IntC32)
                //    UnsafeNativeMethods.leveldb_options_set_max_open_files_32(_options, MaxOpenFiles);
                //else
                //    UnsafeNativeMethods.leveldb_options_set_max_open_files_64(_options, MaxOpenFiles);
            }
            if (BlockSize != kDefaultBlockSize)
                UnsafeNativeMethods.leveldb_options_set_block_size(_options, (IntPtr)BlockSize);
            if (BlockRestartInterval != kDefaultBlockRestartInterval)
            {
                UnsafeNativeMethods.leveldb_options_set_block_restart_interval(_options, BlockRestartInterval);
                //if (UnsafeNativeMethods.IntC32)
                //    UnsafeNativeMethods.leveldb_options_set_block_restart_interval_32(_options, BlockRestartInterval);
                //else
                //    UnsafeNativeMethods.leveldb_options_set_block_restart_interval_64(_options, BlockRestartInterval);
            }
            if (CompressionMode != LevelDb.CompressionMode.Snappy)
            {
                UnsafeNativeMethods.leveldb_options_set_compression(_options, (int)CompressionMode);
                //if (UnsafeNativeMethods.IntC32)
                //    UnsafeNativeMethods.leveldb_options_set_compression32(_options, (int)CompressionMode);
                //else
                //    UnsafeNativeMethods.leveldb_options_set_compression64(_options, (long)CompressionMode);
            }
            if (CacheSizeMb > 0)
            {
                _cachePointer = UnsafeNativeMethods.leveldb_cache_create_lru((IntPtr)(CacheSizeMb * 1048576));
                UnsafeNativeMethods.leveldb_options_set_cache(_options, _cachePointer);
            }
            if (BloomFilterPolicy > 0)
            {
                _filterPointer = UnsafeNativeMethods.leveldb_filterpolicy_create_bloom((IntPtr)BloomFilterPolicy);
                UnsafeNativeMethods.leveldb_options_set_filter_policy(_options, _filterPointer);
            }
            return _options;
        }

        internal void DestroyPointer()
        {
            UnsafeNativeMethods.leveldb_options_destroy(_options);
        }

        /// <summary>
        /// D�truit le cache, les Comparison et Filter. Malgr� son nom n'est pas IDispose.Dispose.
        /// </summary>
        internal void Dispose()
        {
            // FIXME: refcount ? Bah, 1 options par db on a dit.
            if (Comparison != null)
            {
                Comparison.DestroyPointer();
                Comparison = null;
            }
            if (_cachePointer != IntPtr.Zero)
                UnsafeNativeMethods.leveldb_cache_destroy(_cachePointer);
            if (_filterPointer != IntPtr.Zero)
                UnsafeNativeMethods.leveldb_filterpolicy_destroy(_filterPointer);
            _cachePointer = IntPtr.Zero;
            _filterPointer = IntPtr.Zero;
        }

        /// <summary>
        /// Debug string with (some) options.
        /// </summary>
        /// <returns></returns>
        public string DbgImage()
        {
            string res = string.Format("blocksize : {0}, snappy : {1}, wbsize (MB) : {2}, blockrestartinterval : {3}",
                BlockSize,
                CompressionMode == LevelDb.CompressionMode.Snappy,
                WriteBufferSize == 0 ? 4 : WriteBufferSizeMb,
                BlockRestartInterval

                );
            return res;
        }
    }
}
