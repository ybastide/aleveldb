// Copyright (c) 2012-2013 The ALevelDb Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file. See the AUTHORS file for names of contributors.

using System;

namespace ALevelDb
{
    /// <summary>
    /// Options d'�criture.
    /// </summary>
    public class LevelDbWriteOptions
    {
        internal static readonly IntPtr NoSyncWriteOption;
        internal static readonly IntPtr SyncWriteOption;

        /// <summary>
        /// Options d'�criture par d�faut.
        /// </summary>
        public static readonly LevelDbWriteOptions Default = new LevelDbWriteOptions();

        static LevelDbWriteOptions()
        {
            NoSyncWriteOption = UnsafeNativeMethods.leveldb_writeoptions_create();
            SyncWriteOption = UnsafeNativeMethods.leveldb_writeoptions_create();
            UnsafeNativeMethods.leveldb_writeoptions_set_sync(SyncWriteOption, 1);
        }

        /// <summary>
        /// Mode synchro (faux par d�faut).
        /// </summary>
        public bool Sync { get; set; }

        /// <summary>
        /// Renvoie un pointeur vers les options d'�criture. Ne pas d�truire si IsMine (statique).
        /// </summary>
        /// <returns></returns>
        internal IntPtr GetPointer()
        {
            return Sync ? SyncWriteOption : NoSyncWriteOption;
        }

        /// <summary>
        /// Pointeur statique ?
        /// </summary>
        /// <param name="ptr"></param>
        /// <returns></returns>
        internal static bool IsMine(IntPtr ptr)
        {
            return ptr == NoSyncWriteOption || ptr == SyncWriteOption;
        }
    }
}