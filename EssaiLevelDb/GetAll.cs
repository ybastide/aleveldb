﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using ALevelDb;
using TswBase;
using XA;

namespace EssaiLevelDb
{
    [XA]
    public class GetAll
    {
        private Stopwatch _sw;
        private CAsciiGauge _ag;
        private int _nag;
        private int _nkws;
        private int _nlines;
        private long _maxValue;
        private long _bytes;

        [XA]
        public void get_all_kws(CFileInMultiple input
                        , [Explicit(".")] string dir
            )
        {
            _sw = new Stopwatch();
            using (var dbUrl = new LevelDb(Path.Combine(dir, "murls"), new LevelDbOptions { BloomFilterPolicy = 10 }))
            using (var dbKws = new LevelDb(Path.Combine(dir, "mkws"), new LevelDbOptions { BloomFilterPolicy = 10 }))

            using (var reader = new MerlinLineReader(input))
            {
                _maxValue = input.Length;
                _ag = new CAsciiGauge { Title = "select", MaxValue = _maxValue, BreakEverySeconds = 10 };
                _nag = 0;
                _nkws = 0;
                _nlines = 0;
                string kws = null;
                _sw.Start();
                double ms;
                _bytes = 0;
                foreach (MerlinLine ml in reader)
                {
                    if ((_nag % 50000) == 0)
                    {
                        ms = _sw.ElapsedMilliseconds / 1000.0;
                        _ag.Set(_maxValue == 0 ? 0 : input.Position,
                               string.Format("{0:N0} kws {1:N0} lignes ; {2:N0} kws/s {3:N0} l/s ; {4:N0} MB/s", _nkws,
                                             _nlines, _nkws / ms, _nlines / ms, _bytes / ms / (1 << 20)));
                    }
                    _nag++;
                    if (ml != MerlinLine.EndOfRecord)
                    {
                        if (ml.Type == "kws")
                        {
                            kws = ml.Keyword;
                            _nkws++;
                        }
                    }
                    else if (kws != null)
                    {
                        var kwsValue = new byte[1024];
                        int len;
                        if (dbKws.TryGet(kws, ref kwsValue, 0, out len))
                            GetRecord(dbUrl, kws, kwsValue, len, _maxValue == 0 ? 0 : input.Position);
                        else
                        {
                            Console.Error.WriteLine("{0}: non trouvé", kws);
                            throw new Exception();
                        }
                    }
                }
                ms = _sw.ElapsedMilliseconds / 1000.0;
                Console.Error.WriteLine();
                _ag.Finish(
                    string.Format("{0:N0} kws {1:N0} lignes ; {2:N0} kws/s {3:N0} l/s ; {4:N0} MB/s", _nkws, _nlines,
                                  _nkws / ms, _nlines / ms, _bytes / ms / (1 << 20)));
            }
        }

        // get_all_kws2
        [XA]
        public void get_all_kws2([Explicit(".")] string dir
            )
        {
            _sw = new Stopwatch();
            using (var dbUrl = new LevelDb(Path.Combine(dir, "murls"), new LevelDbOptions { BloomFilterPolicy = 10 }))
            using (var dbKws = new LevelDb(Path.Combine(dir, "mkws"), new LevelDbOptions { BloomFilterPolicy = 10 }))
            {
                _maxValue = 1000000;
                _ag = new CAsciiGauge { Title = "select", MaxValue = _maxValue, BreakEverySeconds = 10 };
                _nag = 0;
                _nkws = 0;
                _nlines = 0;
                _sw.Start();
                double ms;
                _bytes = 0;
                foreach (var kv in dbKws.GetIterator2())
                {
                    _nkws++;
                    if ((_nag % 100) == 0)
                    {
                        ms = _sw.ElapsedMilliseconds / 1000.0;
                        _ag.Set(_nag,
                               string.Format("{0:N0} kws {1:N0} lignes ; {2:N0} kws/s {3:N0} l/s ; {4:N0} B/s", _nkws,
                                             _nlines, _nkws / ms, _nlines / ms, _bytes / ms));
                    }
                    _nag++;

                    GetRecord(dbUrl, Encoding.UTF8.GetString(kv.Key), kv.Value, kv.Value.Length, _nag);

                }
                ms = _sw.ElapsedMilliseconds / 1000.0;
                Console.Error.WriteLine();
                _ag.Finish(
                    string.Format("{0:N0} kws {1:N0} lignes ; {2:N0} kws/s {3:N0} l/s ; {4:N0} B/s", _nkws, _nlines,
                                  _nkws / ms, _nlines / ms, _bytes / ms));
            }
        }

        private void GetRecord(LevelDb dbUrl, string kws, byte[] bkValue, int len, long pos)
        {
            _bytes += len + kws.Length;
            var kwsValue = KwsValue.FromBytes(bkValue);
            var bit = new byte[16];
            var hkws = CHash.Hash(kws);
            G.PutBytes(hkws, bit, 12);
            var value = new byte[1024];
            foreach (var hashUrlType in kwsValue.UHashes)
            {
                hashUrlType.ToBytes(bit);
                int vlen;
                if (!dbUrl.TryGet(bit, bit.Length, ref value, 0, out vlen))
                    throw new Exception("not found: " + hashUrlType);
                _nlines++;
                if ((_nag % 100) == 0)
                {
                    double ms = _sw.ElapsedMilliseconds / 1000.0;
                    _ag.Set(pos,
                            string.Format("{0:N0} kws {1:N0} lignes ; {2:N0} kws/s {3:N0} l/s ; {4:N0} B/s", _nkws,
                                          _nlines, _nkws / ms, _nlines / ms, _bytes / ms));
                }
                _nag++;
                _bytes += bit.Length;
                _bytes += vlen;
                //foreach (var kvUrl in dbUrl.GetIterator2(bit))
                // {
                //     var uKey = kvUrl.Key;
                //     if (!HashUrlType.IsEqual(bit, uKey))
                //         break;
                //     _nlines++;
                //     if ((_nag % 5000) == 0)
                //     {
                //         double ms = _sw.ElapsedMilliseconds / 1000.0;
                //         _ag.Set(pos,
                //                 string.Format("{0:N0} kws {1:N0} lignes ; {2:N0} kws/s {3:N0} l/s ; {4:N0} MB/s", _nkws,
                //                               _nlines, _nkws / ms, _nlines / ms, _bytes / ms / (1 << 20)));
                //     }
                //     _nag++;
                //     _bytes += uKey.Length;
                //     _bytes += kvUrl.Value.Length;
                //     Console.WriteLine("{0}\t{1}", kws, Encoding.UTF8.GetString(kvUrl.Value));
                // }
            }
        }

    }
}
