﻿using System.Collections.Generic;

namespace EssaiLevelDb
{
    class NullDbInserter : IDbInserter
    {
        public void Dispose()
        {
        }

        public int WriteLines(MerlinLine ml, int hkws, List<HashUrlType> uHashes)
        {
            return 0;
        }

        public int WriteKws(string kws, KwsValue kwsValue)
        {
            return 0;
        }

        public void DbKwsWriteBatchCommit()
        {
        }

        public void DbUrlWriteBatchCommit()
        {
        }

        public void DbUrlWriteBatchClose(bool b)
        {
        }

        public void DbKwsWriteBatchClose(bool b)
        {
        }
    }
}
