﻿using System;
using System.Collections.Generic;
using Npgsql;

namespace EssaiLevelDb
{
    internal class PgDbInserter : IDbInserter
    {
        private readonly NpgsqlConnection _connection;

        public PgDbInserter()
        {
            _connection = new NpgsqlConnection("database=test");
        }
        public void Dispose()
        {
            _connection.Dispose();
        }

        public int WriteLines(MerlinLine ml, int hkws, List<HashUrlType> uHashes)
        {
            throw new NotImplementedException();
        }

        public int WriteKws(string kws, KwsValue kwsValue)
        {
            throw new NotImplementedException();
        }

        public void DbKwsWriteBatchCommit()
        {
            throw new NotImplementedException();
        }

        public void DbUrlWriteBatchCommit()
        {
            throw new NotImplementedException();
        }

        public void DbUrlWriteBatchClose(bool b)
        {
            throw new NotImplementedException();
        }

        public void DbKwsWriteBatchClose(bool b)
        {
            throw new NotImplementedException();
        }
    }
}