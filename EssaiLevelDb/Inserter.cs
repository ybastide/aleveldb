﻿using System;
using System.Diagnostics;
using TswBase;
using XA;

namespace EssaiLevelDb
{
    [XA]
    public class Inserter
    {
        // insert F:\Apo\Merlin\kws-es-1M-201303.clean.txt
        // insert E:\zeb\Apo\Merlin\test.20130312.google_search_fr_1.done.txt
        // insert E:/zeb/Apo/Merlin/kws-es-1M-201303.clean.txt --count=50
        // insert E:\zeb\Apo\Merlin\k1.txt

        [XA, Help("insert.")]
        public void insert(
            [Explicit(".")] string dir
            , CFileInMultiple input
            , [Explicit(0)] int count
            , [Explicit(true)] bool act
            , [Explicit(DbType.Normal)] DbType type
            )
        {
            if (count <= 0)
                count = int.MaxValue;
            var sw = new Stopwatch();
            if (!act)
                type = DbType.None;
            using (var dbInserter = CreateDbInserter(dir, type))
            using (var reader = new MerlinLineReader(input))
            {
                long maxValue = input.Length;
                var ag = new CAsciiGauge { Title = "Insertion", MaxValue = maxValue, BreakEverySeconds = 10 };
                int nag = 0;
                int nkws = 0;
                int nlines = 0;
                string kws = null;
                int hkws = 0;
                var kwsValue = new KwsValue();
                sw.Start();
                double ms;
                long bytes = 0;
                foreach (MerlinLine ml in reader)
                {
                    if ((nag % 50000) == 0)
                    {
                        ms = sw.ElapsedMilliseconds / 1000.0;
                        ag.Set(maxValue == 0 ? 0 : input.Position,
                               string.Format("{0:N0} kws {1:N0} lignes ; {2:N0} kws/s {3:N0} l/s ; {4:N0} MB/s", nkws,
                                             nlines, nkws / ms, nlines / ms, bytes / ms / (1 << 20)));
                    }
                    nag++;
                    if (ml != MerlinLine.EndOfRecord)
                    {
                        if (ml.Type == "kws")
                        {
                            kws = ml.Keyword;
                            hkws = CHash.Hash(kws);
                            kwsValue = new KwsValue();
                            nkws++;
                            if ((nkws % 1000) == 0)
                            {
                                dbInserter.DbKwsWriteBatchCommit();
                            }
                        }
                        else if (ml.Type == "nbt")
                        {
                            kwsValue.Nbt = int.Parse(ml.FieldValues[0]);
                        }
                        else if (ml.Url != null || ml.Keyword != null)
                        {
                            bytes += dbInserter.WriteLines(ml, hkws, kwsValue.UHashes);
                            nlines++;
                            if ((nlines % 50000) == 0)
                            {
                                dbInserter.DbUrlWriteBatchCommit();
                            }
                        }
                    }
                    else if (kws != null)
                    {
                        bytes += dbInserter.WriteKws(kws, kwsValue);
                        kws = null;
                        if (nkws >= count)
                            break;
                    }
                }
                ms = sw.ElapsedMilliseconds / 1000.0;
                dbInserter.DbUrlWriteBatchClose(true);
                dbInserter.DbKwsWriteBatchClose(true);
                Console.Error.WriteLine();
                ag.Finish(
                    string.Format("{0:N0} kws {1:N0} lignes ; {2:N0} kws/s {3:N0} l/s ; {4:N0} MB/s", nkws, nlines,
                                  nkws / ms, nlines / ms, bytes / ms / (1 << 20)));
            }
        }

        private static IDbInserter CreateDbInserter(string dir, DbType type)
        {
            switch (type)
            {
                case DbType.Normal:
                    return new LevelDbInserter(dir);
                  
                case DbType.None:
                    return new NullDbInserter();
                case DbType.File:
                    return new BinaryDbInserter(dir);
                case DbType.Pg:
                    return new PgDbInserter();
                default:
                    throw new ArgumentOutOfRangeException("type");
            }

        }
    }

    public enum DbType
    {
        Normal,
        None,
        File,
        Pg,
    }

}