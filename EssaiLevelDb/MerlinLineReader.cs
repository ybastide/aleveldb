﻿using System;
using System.Collections;
using System.Collections.Generic;
using TswBase;

namespace EssaiLevelDb
{
    public class MerlinLine
    {
        public static MerlinLine EndOfRecord;

        public string Line { get; private set; }
        public string Type { get; private set; }
        public string FieldTypes { get; private set; }
        public string[] FieldValues { get; private set; }

        /// <summary>
        /// Url de la ligne ou null.
        /// </summary>
        public string Url { get; private set; }

        /// <summary>
        /// !kws de la ligne ou null.
        /// </summary>
        public string Keyword { get; private set; }

        public MerlinLine(string line, string type, string fieldTypes, string[] fieldValues)
        {
            Line = line;
            Type = type;
            FieldTypes = fieldTypes;
            FieldValues = fieldValues;
            for (int i = 0; i < fieldTypes.Length; i++)
                switch (fieldTypes[i])
                {
                    case 'U':
                        Url = fieldValues[i];
                        break;
                    case 'K':
                        Keyword = fieldValues[i];
                        break;
                }
        }
    }

    public class MerlinLineReader : IEnumerable<MerlinLine>, IDisposable
    {
        private readonly IFileIn _input;

        public MerlinLineReader(IFileIn input)
        {
            _input = input;
        }

        public IEnumerator<MerlinLine> GetEnumerator()
        {
            if (!_input.IsOpen)
                _input.Open();
            bool start = false;
            foreach (var line in _input)
            {
                if (line.Length == 0)
                    continue;
                if (line[0] == '[' && line.Length >= 5)
                {
                    if (line[1] == 'M' && line[2] == 'E' && line[3] == 'R')
                        start = true;
                    else if (line[1] == '/' && line[2] == 'M' && line[3] == 'E' && line[4] == 'R')
                    {
                        start = false;
                        yield return MerlinLine.EndOfRecord;
                    }
                    continue;
                }
                if (!start)
                    continue;
                int p0 = line.IndexOf('\t');
                if (p0 < 0)
                    continue;
                int p1 = line.IndexOf(':', 0, p0);
                string type = line.Substring(0, p1 >= 0 ? p1 : p0);
                string fieldTypes = p1 < 0 ? "" : line.Substring(p1 + 1, p0 - p1);
                if (type == "kws")
                    fieldTypes = "K";
                string[] fieldValues = line.Substring(p0 + 1).Split('\t');
                yield return new MerlinLine(line, type, fieldTypes, fieldValues);
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Dispose()
        {
            _input.Dispose();
        }
    }
}
