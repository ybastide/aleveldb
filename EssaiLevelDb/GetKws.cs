﻿using System;
using System.IO;
using System.Text;
using ALevelDb;
using TswBase;
using XA;

namespace EssaiLevelDb
{
    [XA]
    public class GetKws
    {
        // get-kws *
        // get-kws preescolar
        [XA, Help("get by kws.")]
        public void get_kws(string kws
            , [Explicit("-")] CFileOut output
            , [Explicit(".")] string dir
            )
        {
            bool startswith;
            if (kws.EndsWith("*"))
            {
                startswith = true;
                kws = kws.Substring(0, kws.Length - 1);
            }
            else
            {
                startswith = false;
            }

            using (var dbUrl = new LevelDb(Path.Combine(dir, "murls"), new LevelDbOptions { BloomFilterPolicy = 10, ParanoidChecks = true }))
            using (var dbKws = new LevelDb(Path.Combine(dir, "mkws"), new LevelDbOptions { BloomFilterPolicy = 10, ParanoidChecks = true }))
            using (output.Open())
            {
                if (!startswith)
                {
                    var kwsValue = new byte[1024];
                    int len;
                    if (dbKws.TryGet(kws, ref kwsValue, 0, out len))
                        SaveRecord(dbUrl, output, kws, kwsValue);
                    else
                        Console.Error.WriteLine("{0}: non trouvé", kws);
                }
                else
                {
                    using (var iterator = dbKws.GetIterator(kws))
                    {
                        foreach (var kvKws in iterator)
                        {
                            var kwsKey = Encoding.UTF8.GetString(kvKws.Key);
                            byte[] kwsValue = kvKws.Value;
                            bool ok = !startswith ? kwsKey == kws : kwsKey.StartsWith(kws);
                            if (!ok)
                                break;
                            SaveRecord(dbUrl, output, kwsKey, kwsValue);
                        }
                    }
                }
            }
        }

        private static void SaveRecord(LevelDb dbUrl, CFileOut output, string kws, byte[] kValue)
        {
            var kwsValue = KwsValue.FromBytes(kValue);
            output.WriteLine("kws\t{0}", kws);
            output.WriteLine("nbt\t{0}", kwsValue.Nbt);
            var bit = new byte[16];
            var hkws = CHash.Hash(kws);
            G.PutBytes(hkws, bit, 12);
            var value = new byte[1024];
            foreach (var hashUrlType in kwsValue.UHashes)
            {
                hashUrlType.ToBytes(bit);
                int vlen;
                if (!dbUrl.TryGet(bit, bit.Length, ref value, 0, out vlen))
                    throw new Exception("not found: " + hashUrlType);
                output.WriteLine(Encoding.UTF8.GetString(value, 0, vlen));
                //using (var iterator = dbUrl.GetIterator(bit))
                //{
                //    foreach (var kvUrl in iterator)
                //    {
                //        var uKey = kvUrl.Key;
                //        if (!ArrayEqual(bit, uKey))
                //            break;
                //        //if (!HashUrlType.IsEqual(bit, uKey))
                //        //    continue;
                //        output.WriteLine(Encoding.UTF8.GetString(kvUrl.Value));
                //    }
                //}
            }
        }

        public static bool ArrayEqual(byte[] myBytes, byte[] bytes)
        {
            int len = myBytes.Length;
            if (len != bytes.Length)
                return false;
            for (int i = 0; i < myBytes.Length; i++)
            {
                if (myBytes[i] != bytes[i])
                    return false;
            }
            return true;
        }

    }
}
