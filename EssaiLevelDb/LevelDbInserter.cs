﻿using System;
using System.IO;
using ALevelDb;

namespace EssaiLevelDb
{
    public class LevelDbInserter : DbInserterBase, IDbInserter
    {
        readonly ILevelDb _dbUrl;
        readonly ILevelDb _dbKws;

        public LevelDbInserter(string dir)
        {
            _dbUrl = new LevelDb(Path.Combine(dir, "murls"),
                                 new LevelDbOptions
                                     {
                                         CreateIfMissing = true,
                                         BloomFilterPolicy = 10,
                                         ParanoidChecks = true
                                     });
            _dbKws = new LevelDb(Path.Combine(dir, "mkws"),
                                 new LevelDbOptions
                                     {
                                         CreateIfMissing = true,
                                         BloomFilterPolicy = 10,
                                         ParanoidChecks = true
                                     });
            _dbUrl.WriteBatchCreate();
            _dbKws.WriteBatchCreate();
        }

        protected override void PutUrl(int vlen)
        {
            _dbUrl.Put(BufLineK, BufLineK.Length, BufLineV, vlen);
        }

        protected override void PutKws(KwsValue kwsValue, int klen)
        {
            _dbKws.Put(BufKwsK, klen, BufKwsV, kwsValue.Size);
        }

        public void Dispose()
        {
            _dbUrl.Dispose();
            _dbKws.Dispose();
        }

        public void DbKwsWriteBatchCommit()
        {
            _dbKws.WriteBatchCommit();
        }

        public void DbUrlWriteBatchCommit()
        {
            _dbUrl.WriteBatchCommit();
        }

        public void DbUrlWriteBatchClose(bool b)
        {
            _dbUrl.WriteBatchClose(b);
        }

        public void DbKwsWriteBatchClose(bool b)
        {
            _dbKws.WriteBatchClose(b);
        }
    }
}