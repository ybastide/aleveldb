using System.Diagnostics;
using TswBase;

namespace EssaiLevelDb
{
    public struct HashUrlType
    {
        public ulong HUrl;
        public int HType;

        public void ToBytes(byte[] buffer)
        {
            G.PutBytes(HUrl, buffer, 0);
            G.PutBytes(HType, buffer, 8);
        }

        public static bool IsEqual(byte[] myBytes, byte[] bytes)
        {
            for (int i = 0; i < 12; i++)
            {
                if (myBytes[i] != bytes[i])
                    return false;
            }
            return true;
        }

        public override string ToString()
        {
            return string.Format("url={0},type={1}", HUrl, HType);
        }
    }
}