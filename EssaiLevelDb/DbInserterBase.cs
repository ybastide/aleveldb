using System.Collections.Generic;
using System.Text;
using TswBase;

namespace EssaiLevelDb
{
    public abstract class DbInserterBase
    {
        /// <summary>
        ///     K : Hash64(U ou K) Hash(Type) Hash(kws)
        /// </summary>
        protected readonly byte[] BufLineK = new byte[16];

        /// <summary>
        ///     K : kws
        /// </summary>
        protected byte[] BufKwsK = new byte[4096];

        /// <summary>
        ///     V : nbt int(0) uhashes
        /// </summary>
        /// <remarks>int(0) : pour mettre dt si nécessaire</remarks>
        protected byte[] BufKwsV = new byte[4096];

        /// <summary>
        ///     V : ligne
        /// </summary>
        protected byte[] BufLineV = new byte[4096];

        public int WriteLines(MerlinLine ml, int hkws, List<HashUrlType> uHashes)
        {
            string k = ml.Url ?? ml.Keyword;
            ulong hUrl = CHash.Hash64(k);
            int hType = CHash.Hash(ml.Type);
            G.PutBytes(hUrl, BufLineK, 0);
            G.PutBytes(hType, BufLineK, 8);
            G.PutBytes(hkws, BufLineK, 12);
            uHashes.Add(new HashUrlType { HUrl = hUrl, HType = hType });
            int vlen = Encoding.UTF8.GetByteCount(ml.Line);
            if (vlen > BufLineV.Length)
                BufLineV = new byte[vlen];
            vlen = Encoding.UTF8.GetBytes(ml.Line, 0, ml.Line.Length, BufLineV, 0);
            PutUrl(vlen);
            return BufLineK.Length + vlen;
            //return 1;
        }

        public int WriteKws(string kws, KwsValue kwsValue)
        {
            int klen = Encoding.UTF8.GetByteCount(kws);
            if (klen > BufKwsK.Length)
                BufKwsK = new byte[klen];
            klen = Encoding.UTF8.GetBytes(kws, 0, kws.Length, BufKwsK, 0);

            kwsValue.PutBytes(ref BufKwsV);
            PutKws(kwsValue, klen);
            return klen + kwsValue.Size;
        }

        protected abstract void PutUrl(int vlen);
        protected abstract void PutKws(KwsValue kwsValue, int klen);
    }
}