using System;
using System.Collections.Generic;
using TswBase;

namespace EssaiLevelDb
{
    public class KwsValue
    {
        public int Nbt;
        public int Dt;
        public List<HashUrlType> UHashes = new List<HashUrlType>();

        public void PutBytes(ref byte[] buffer)
        {
            int vlen = Size;
            if (vlen > buffer.Length)
                buffer = new byte[vlen];
            G.PutBytes(Nbt, buffer, 0);
            G.PutBytes(Dt, buffer, 4);
            for (int i = 0; i < UHashes.Count; i++)
            {
                G.PutBytes(UHashes[i].HUrl, buffer, 8 + i * 12);
                G.PutBytes(UHashes[i].HType, buffer, 8 + i * 12 + 8);
            }
        }

        public int Size
        {
            get { return 8 + UHashes.Count * 12; }
        }

        public static KwsValue FromBytes(byte[] buffer)
        {
            var kwsValue = new KwsValue();
            kwsValue.Nbt = BitConverter.ToInt32(buffer, 0);
            kwsValue.Dt = BitConverter.ToInt32(buffer, 4);
            int nb = (buffer.Length - 8) / 12;
            kwsValue.UHashes = new List<HashUrlType>();
            for (int i = 0; i < nb; i++)
                kwsValue.UHashes.Add(new HashUrlType
                    {
                        HUrl = BitConverter.ToUInt64(buffer, 8 + i * 12),
                        HType = BitConverter.ToInt32(buffer, 8 + i * 12 + 8)
                    });
            return kwsValue;
        }
    }
}