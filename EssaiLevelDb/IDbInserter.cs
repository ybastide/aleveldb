﻿using System;
using System.Collections.Generic;

namespace EssaiLevelDb
{
    public interface IDbInserter : IDisposable
    {
        int WriteLines(MerlinLine ml, int hkws, List<HashUrlType> uHashes);
        int WriteKws(string kws, KwsValue kwsValue);
        void DbKwsWriteBatchCommit();
        void DbUrlWriteBatchCommit();
        void DbUrlWriteBatchClose(bool b);
        void DbKwsWriteBatchClose(bool b);
    }
}