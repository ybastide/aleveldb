﻿using System.IO;

namespace EssaiLevelDb
{
    class BinaryDbInserter : DbInserterBase, IDbInserter
    {
        private readonly BinaryDb _kws;
        private readonly BinaryDb _url;

        public BinaryDbInserter(string dir)
        {
            _kws = new BinaryDb(Path.Combine(dir, "mkws.bin"));
            _url = new BinaryDb(Path.Combine(dir, "murls.bin"));
        }
        public void Dispose()
        {
            _kws.Dispose();
            _url.Dispose();
        }

        protected override void PutUrl(int vlen)
        {
            _url.Put(BufLineK, BufLineK.Length, BufLineV, vlen);
        }

        protected override void PutKws(KwsValue kwsValue, int klen)
        {
            _kws.Put(BufKwsK, klen, BufKwsV, kwsValue.Size);
        }

        public void DbKwsWriteBatchCommit()
        {
            _kws.WriteBatchCommit();
        }

        public void DbUrlWriteBatchCommit()
        {
            _url.WriteBatchCommit();
        }

        public void DbUrlWriteBatchClose(bool b)
        {
            _url.WriteBatchClose(b);
        }

        public void DbKwsWriteBatchClose(bool b)
        {
            _kws.WriteBatchClose(b);
        }
    }
}
