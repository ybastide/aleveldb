﻿using System;
using System.IO;
using ALevelDb;
using TswBase;

namespace EssaiLevelDb
{
    class BinaryDb
    {
        private readonly Stream _stream;
        private readonly byte[] _buffer = new byte[1 << 20];
        private int _pos;

        public BinaryDb(string path)
        {
            _stream = File.OpenWrite(path);
        }

        public void Dispose()
        {
            _stream.Dispose();
        }

        public void WriteBatchCreate()
        {
        }

        public void WriteBatchClose(bool commit = false, LevelDbWriteOptions writeOptions = null)
        {
            if (commit)
                WriteBatchCommit(writeOptions);
        }

        public void WriteBatchCancel()
        {
            _pos = 0;
        }

        public void WriteBatchCommit(LevelDbWriteOptions writeOptions = null)
        {
            _stream.Write(_buffer, 0, _pos);
            _pos = 0;
        }

        public void Put(byte[] key, int klen, byte[] value, int vlen)
        {
            int len = klen + 4 + vlen + 4;
            if (_pos + len > _buffer.Length)
                WriteBatchCommit();
            G.PutBytes(klen, _buffer, _pos);
            _pos += 4;
            Array.Copy(key, 0, _buffer, _pos, klen);
            _pos += klen;
            G.PutBytes(vlen, _buffer, _pos);
            _pos += 4;
            Array.Copy(value, 0, _buffer, _pos, vlen);
            _pos += vlen;
        }
    }
}