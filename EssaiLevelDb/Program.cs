﻿using XA;

namespace EssaiLevelDb
{
    class Program
    {
        static void Main(string[] args)
        {
            var runner = new CXARunner(false);
            runner.CmdLineRun(args);
        }
    }
}
